<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member')->nullable();
            $table->foreign('member')->references('id')->on('members')->onDelete('cascade');
            $table->unsignedBigInteger('branch')->nullable();
            $table->foreign('branch')->references('id')->on('branches')->onDelete('cascade');
            $table->text('text')->nullable();
            $table->text('hulle')->nullable();
            $table->text('tasche')->nullable();
            $table->text('glas')->nullable();
            $table->text('zubehhor')->nullable();
            $table->text('kasse')->nullable();
            $table->text('bar')->nullable();
            $table->text('karte')->nullable();
            $table->string('status_shop',20)->default('0');
            $table->text('price_mode')->nullable();
            $table->string('range')->nullable();
            $table->timestamp('start');
            $table->timestamp('end')->nullable();
            $table->string('start_ip')->nullable();
            $table->string('end_ip')->nullable();
            $table->string('start_longitude')->nullable();
            $table->string('start_latitude')->nullable();
            $table->string('end_longitude')->nullable();
            $table->string('end_latitude')->nullable();
            $table->string('income')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_reports');
    }
}
