<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberReportBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_report_breaks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_report');
            $table->foreign('member_report')->references('id')->on('member_reports')->onDelete('cascade');
            $table->text('text');
            $table->string('range')->nullable();
            $table->timestamp('start');
            $table->timestamp('end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_report_breaks');
    }
}
