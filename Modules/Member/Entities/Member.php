<?php

namespace Modules\Member\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticate;
use Laravel\Passport\HasApiTokens;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\Wallet;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Seo\Entities\Seo;
use Modules\Core\Entities\Branch;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Member extends  Authenticate implements HasMedia
{
    use HasApiTokens,HasTags,HasMediaTrait,Sluggable,TimeAttribute,Notifiable;

    protected $guard='member';

    protected  $table='members';

    protected $fillable = [
        'first_name',
        'last_name',
        'status',
        'ip',
        'password',
        'country',
        'order',
        'is_active',
        'role',
        'token',
        'slug',
        'mobile',
        'email',
        'user',
        'role',
        'branch'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public  function role(){
        return $this->hasOne(MemberRole::class,'id','role');
    }
    public  function branch(){
        return $this->hasOne(Branch::class,'id','branch');
    }

    public  function professor(){
        return $this->hasOne(MemberRole::class,'id','role')->where('title','=','professor');
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'last_name'
            ]
        ];
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

    }
    public  function getRoleNameAttribute(){
        return __('cms.'.$this->role()->first()->title);
    }

    public  function getAllKarteAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('karte')->pluck('karte')->toArray());
    }
    public  function getAllBarAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('bar')->pluck('bar')->toArray());
    }
    public  function getAllKasseAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('kasse')->pluck('kasse')->toArray());
    }

    public  function getAllYesterdayKarteAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('karte')->pluck('karte')->toArray());
    }
    public  function getAllYesterdayBarAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('bar')->pluck('bar')->toArray());
    }
    public  function getAllYesterdayKasseAttribute(){
        return calcEuro(MemberReport::where('member',$this->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('kasse')->pluck('kasse')->toArray());
    }

    public  function getAllTimeAttribute(){
        // $h=0;
        // $m=0;
        $t_hour=checkAllTimeReportHour($this->id);
        $t_min=checkAllTimeReportMinu($this->id);
        // $hour=calcAllTimeReportHour($this->id);
        // $min=calcAllTimeReportMinu($this->id);
        // if(calcAllTimeReportMinu($this->id) > 60){

        //     $h=calcAllTimeReportMinu($this->id) / 60;
        //     $m=calcAllTimeReportMinu($this->id) % 60;
        //     $hour+=floor($h);
        //     $min=$m;


        // }
        return ($t_hour+$t_min['mode2']).":".($t_min['mode1']);    }

    public  function getAllBreakAttribute(){
       
        $h=0;
        $m=0; 
        $hour=calcAllTimeBreakHour($this->id);
        $min=calcAllTimeBreakMinu($this->id);
        if(calcAllTimeBreakMinu($this->id) > 60){

            $h=calcAllTimeBreakMinu($this->id) / 60;
            $m=calcAllTimeBreakMinu($this->id) % 60;
            $hour+=floor($h);
            $min=$m;


        }
        return $hour.":".$min;
    }
    public  function getAllYesterdayTimeAttribute(){
        // $h=0;
        // $m=0;
        $t_hour=checkAllYesterdayTimeReportHour($this->id);
        $t_min=checkAllYesterdayTimeReportMinu($this->id);
        // $hour=calcAllYesterdayTimeReportHour($this->id);
        // $min=calcAllYesterdayTimeReportMinu($this->id);
        // if(calcAllYesterdayTimeReportMinu($this->id) > 60){

        //     $h=calcAllYesterdayTimeReportMinu($this->id) / 60;
        //     $m=calcAllYesterdayTimeReportMinu($this->id) % 60;
        //     $hour+=floor($h);
        //     $min=$m;


        // }
        return ($t_hour+$t_min['mode2']).":".($t_min['mode1']);
    }

   

    public  function getFullNameAttribute(){
        return $this->first_name." ".$this->last_name;
    }




}
