<?php

namespace Modules\Member\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberReportBreak extends Model
{
    protected $table = 'member_report_breaks';
    protected $fillable = ['member_report','text','range','start','end'];

    
   
}
