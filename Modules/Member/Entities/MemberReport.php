<?php

namespace Modules\Member\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Branch;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Modules\Member\Helper\MemberHelper;


class MemberReport extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'member_reports';
    protected $fillable = ['member','hulle','status_shop','price_mode','branch','kasse','karte','bar','tasche','zubehhor','glas','text','start_browser','end_browser','range','start','end','start_ip','end_ip','start_longitude','start_latitude','end_longitude','end_latitude','income'];
  
    public  function member(){
        return $this->hasOne(Member::class,'id','member');
    }
    

    public  function getBranchAttribute(){
        return Branch::find($this->member()->first()->branch)->title;
    }

    public  function getEmailAttribute(){
        return !is_null($this->member) ? $this->member()->first()->email : null;
    }
    public  function getTodayTimeAttribute(){
        return calcTimeReportHour($this->start,$this->end).":".calcTimeReportMinu($this->start,$this->end);
    }
    public  function getYesterdayTimeAttribute(){
        return calcTimeReportHour($this->start,$this->end).":".calcTimeReportMinu($this->start,$this->end);

    }
    public  function getTodayHourAttribute(){
        return calcTimeReportHour($this->start,$this->end);
    }
    public  function getTodayMinuAttribute(){
        return calcTimeReportMinu($this->start,$this->end);
    }
   
    public  function getStartLocationAttribute(){
        return MemberHelper::startLocation($this->id);
    }
    public  function getEndLocationAttribute(){
        return MemberHelper::endLocation($this->id);
    }
    public  function getGetBreakAttribute(){
        return MemberHelper::getBreakTime($this->start,$this->end,$this->id,$this->member()->first()->token);
    }
    

    public  function member_report_break(){
        return $this->hasMany(MemberReportBreak::class,'id','member_report');
    }

    public  function getResultAttribute(){
        if(is_null($this->end)){
            return null;
        }else{
            return calcTimeReportHour($this->start,$this->end).":".calcTimeReportMinu($this->start,$this->end);

           
        }
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }
    
}
