<?php

namespace Modules\Member\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Branch;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Member\Entities\MemberReport;
use Modules\Member\Http\Requests\MemberRequest;
use Carbon\Carbon;

class MemberController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Member();

        $this->middleware('permission:member-list')->only('index');
        $this->middleware('permission:member-create')->only(['create','store']);
        $this->middleware('permission:member-edit' )->only(['edit','update']);
        $this->middleware('permission:member-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            if(auth('web')->user()->branch_access=='0'){
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            }
            else{
                $items=$this->entity->latest()->whereBranch(auth('web')->user()->branch_access)->paginate(config('cms.paginate'));
   
            }
            return view('member::members.index',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function yesterday()
    {
        try {
            
            if(auth('web')->user()->branch_access=='0'){
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            }
            else{
                $items=$this->entity->latest()->whereBranch(auth('web')->user()->branch_access)->paginate(config('cms.paginate'));
   
            }
            return view('member::members.yesterday-index',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    public function onlineMember()
    {
        try {
            if(auth('web')->user()->branch_access=='0'){
                $report=MemberReport::with('member')->latest()->whereDate('start', Carbon::today())->whereNull('end')->pluck('member')->toArray();
                $items=Member::whereIn('id',$report)->get();
               
            }else{
                $report=MemberReport::whereBranch(auth('web')->user()->branch_access)->with('member')->latest()->whereDate('start', Carbon::today())->whereNull('end')->pluck('member')->toArray();
                $items=Member::whereBranch(auth('web')->user()->branch_access)->whereIn('id',$report)->get();
               
            }
           
            return view('member::members.online-member',compact('items'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
           
            !isset($request->email)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('member::members.index',compact('items'));
            }
             $items=$this->entity
             ->where('email', 'like', '%' . trim($request->email) . '%')
             
             
                ->paginate(config('cms.paginate'));
            return view('member::members.index',compact('items','request'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $roles=MemberRole::latest()->get();
            if(auth('web')->user()->branch_access=='0'){
            
                $branches=Branch::latest()->get();
            }else{
              
                $branches=Branch::latest()->whereId(auth('web')->user()->branch_access)->get();
            }
         
            return view('member::members.create',compact('roles','branches'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param MemberRequest $request
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(MemberRequest $request)
    {
        try {

           DB::beginTransaction();
            $this->entity->user=auth('web')->user()->id;
            if(!is_null(trim($request->input('password')))){
                $this->entity->password=Hash::make(trim($request->input('password')));
            }
            $this->entity->email=$request->input('email');
            $this->entity->ip=$request->input('ip');
            $this->entity->is_active=2;
            $this->entity->status=$request->input('status');
            $this->entity->branch=Branch::whereToken($request->input('branch'))->firstOrFail()->id;
            $this->entity->token=tokenGenerate();
            $saved=$this->entity->save();


            $this->entity->info()->create([
              

            ]);


            $this->entity->seo()->create([
              
            ]);
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                DB::commit();
                return redirect(route("members.index"))->with('message',__('member::members.store'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
         
            return abort('500');

        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $roles=MemberRole::latest()->get();
            $branches=Branch::latest()->get();
            return view('member::members.edit',compact('item','roles','branches'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $validator=Validator::make($request->all(),[
                
                'password'=>'nullable|min:9',
                'email'=>'required|unique:members,email,'.$token.',token',
                'branch'=>'required',
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }

            if(is_null($request->input('password'))){
                $update=$this->entity->update([
                    "user"=>auth('web')->user()->id,
                    "email"=>$request->input('email'),
                    'status'=>$request->input('status'),
                    'ip'=>$request->input('ip'),
                    "branch"=>Branch::whereToken($request->input('branch'))->firstOrFail()->id,
                ]);
            }
            else{
                $update=$this->entity->update([
                    "user"=>auth('web')->user()->id,
                    "email"=>$request->input('email'),
                    'status'=>$request->input('status'),
                    'ip'=>$request->input('ip'),
                    "password"=>Hash::make(trim($request->input('password'))),
                    "branch"=>Branch::whereToken($request->input('branch'))->firstOrFail()->id,
                ]);
               
            }


            $this->entity->info()->update([
              

            ]);



            $this->entity->seo()->update([
               
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                DB::rollBack();
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                DB::commit();
                return redirect(route("members.index"))->with('message',__('member::members.update'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
           
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $this->entity->seo()->delete();
            $this->entity->info()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                DB::rollBack();
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                DB::commit();
                return redirect(route("members.index"))->with('message',__('member::members.delete'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
