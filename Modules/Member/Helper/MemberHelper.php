<?php


namespace Modules\Member\Helper;


use Modules\Member\Entities\MemberRole;
use Modules\Member\Entities\MemberReport;
use Modules\Member\Entities\MemberReportBreak;



class MemberHelper
{
    public static function role($role){

                $label=MemberRole::whereId($role)->first()->label;
                return '<span class="status-success">'.$label.'</span>';


    }
    public static function roleName($role){

        return $label=MemberRole::whereId($role)->first()->label;


    }
    public static function startLocation($id){

   
        if(!is_null(MemberReport::whereId($id)->first()->start_latitude)){
            $report=MemberReport::whereId($id)->first();
            return "<a target='_blnk' href='https://maps.google.com/?q=$report->start_latitude,$report->start_longitude'>StartLocation</a>";
            
        }
        else{
            return "<span>No position</span>";
        }

    }
    public static function endLocation($id){

        if(!is_null(MemberReport::whereId($id)->first()->end_latitude)){
            $report=MemberReport::whereId($id)->first();
            return "<a target='_blnk' href='https://maps.google.com/?q=$report->end_latitude,$report->end_longitude'>Endlocation</a>";
        }
        else{
            return "<span>No position</span>";
        }
       


    }

    public static function getBreakTime($start,$end,$id,$token){
        $report=MemberReport::with('member_report_break')->whereId($id)->first();
        // MemberReport::whereMember($member->id)->orderBy('id', 'DESC')->first()
        if(is_null($end)){
            return "<span>00:00</span>";
              
            }
        else
        {
            return "<span>".calcTimeBreakReportHour($token).":".calcTimeBreakReportMinu($token)."</span>";
           
        }
 
    }


}
