@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.members')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.members')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


               
                    <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/Default-welcomer.png')}}"
                       alt="" style="border-radius:unset;width:100%;">

    
                 


                </div>

                <h3 class="profile-username text-center">{{old('title')}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('members.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                     
                    
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('email')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="email" value="{{old('email')}}" class="form-control" id="inputName" placeholder="{{__('cms.email')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('password')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="password" value="{{old('password')}}" class="form-control" id="inputName" placeholder="{{__('cms.password')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('ip')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="ip" value="{{old('ip')}}" class="form-control" id="inputName" placeholder="{{__('cms.ip')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('status')}}</label>
                        <div class="col-sm-10">

                        <select  class="form-control" id="status" name="status" >

<option  value="1" selected>{{__('cms.active')}}</option>
<option  value="0">{{__('cms.inactive')}}</option>


</select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('branch')}}</label>
                        <div class="col-sm-10">
                        <select  class="form-control" name="branch" >

@foreach($branches as $branch)
    <option value="{{$branch->token}}">{{$branch->title}}</option>
@endforeach

</select>
                        </div>
                      </div>

                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                  

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection
 
 
 














