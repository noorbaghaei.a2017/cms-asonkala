@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                           {{__('member::members.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('members.store')}}" enctype="multipart/form-data">
                            @csrf
                           

                            <div class="form-group row">

                               
<div class="col-sm-3">
    <span class="text-danger">*</span>
    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
    <input type="email" name="email" value="{{old('email')}}" class="form-control" id="email">
</div>
<div class="col-sm-3">
    <span class="text-danger">*</span>
    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
    <input type="text" name="password"  class="form-control" id="password" >
</div>
<div class="col-sm-3">
  
    <label for="ip" class="form-control-label">{{__('cms.ip')}} </label>
    <input type="text" name="ip"  class="form-control" id="ip" >
</div>
<div class="col-sm-3">
    <span class="text-danger">*</span>
    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
    <select dir="rtl" class="form-control" id="status" name="status" required>

        <option  value="1" selected>{{__('cms.active')}}</option>
        <option  value="0">{{__('cms.inactive')}}</option>


    </select>
</div>
</div>
                            <div class="form-group row">
                               
                               
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="branch"  class="form-control-label">{{__('cms.branch')}}  </label>
                                    <select dir="rtl" class="form-control" name="branch" required>

                                            @foreach($branches as $branch)
                                                <option value="{{$branch->token}}">{{$branch->title}}</option>
                                            @endforeach

                                    </select>

                                </div>


                               


                            </div>

                           

        


                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.send')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                   
                    email: {
                        required: true,
                        email:true
                    },
                   
                   


                },
                messages: {
                    email: "ایمیل  الزامی است",
                   
                  
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
