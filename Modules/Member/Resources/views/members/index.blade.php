
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.all_members')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.all_members')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
     
      <div class="col-12">

            <div class="card">
            @include('core::layout.alert-success')
            @include('core::layout.alert-danger')
         
              <div class="card-header">
              <a href="{{route('members.create')}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
              <a href="{{ route('yesterday.members.index') }}" class="btn btn-info">Letzte Monate</a>
          
              <br>
<br>
                <h3 class="card-title">{{__('cms.all_members')}}</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="{{__('cms.search')}}">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                    <th style="width: 10px">#</th>
                      <th>{{__('cms.email')}}</th>
                      <th>{{__('cms.total_hour')}}</th>
                      <th>{{__('cms.total_break')}}</th>
                      <th>{{__('cms.kasse')}}</th>
                      <th>{{__('cms.karte')}}</th>
                      <th>{{__('cms.bar')}}</th>
                     
                   
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($items as $key=>$item)
                      
                   
                    <tr>
                      <td>{{++$key}}</td>
                      <td>{{$item->email}}</td>
                      <td>{{$item->all_time}}</td>
                      <td>{{$item->all_break}}</td>



                      @if($item->all_kasse['euro']!='0' && $item->all_kasse['cent']!='0')
                <td >{{number_format($item->all_kasse['euro'])}} Euro {{$item->all_kasse['cent']}} Cent</td>

                @elseif($item->all_kasse['cent']=='0')

                <td >{{number_format($item->all_kasse['euro'])}} Euro</td>
                @elseif($item->all_kasse['euro']=='0')
                <td >{{$item->all_kasse['cent']}} Cent</td>
              
                @else
                <td>0</td>
                @endif



                @if($item->all_karte['euro']!='0' && $item->all_karte['cent']!='0')
                <td >{{number_format($item->all_karte['euro'])}} Euro {{$item->all_karte['cent']}} Cent</td>

                @elseif($item->all_karte['cent']=='0')

                <td >{{number_format($item->all_karte['euro'])}} Euro</td>
                @elseif($item->all_karte['euro']=='0')
                <td >{{$item->all_karte['cent']}} Cent</td>
                @else
                <td>0</td>
                @endif



                @if($item->all_bar['euro']!='0' && $item->all_bar['cent']!='0')
                <td >{{number_format($item->all_bar['euro'])}} Euro {{$item->all_bar['cent']}} Cent</td>

                @elseif($item->all_bar['cent']=='0')

                <td >{{number_format($item->all_bar['euro'])}} Euro</td>
                @elseif($item->all_bar['euro']=='0')
                <td >{{$item->all_bar['cent']}} Cent</td>
               
                @else
                <td>0</td>
                @endif
                     
                    
             
                    </tr>
                   
                    @endforeach
                   
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
       
       
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection








