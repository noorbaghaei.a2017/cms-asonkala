<?php
return [
    "text-create"=>"you can create your order",
    "text-edit"=>"you can edit your order",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"orders list",
    "error"=>"error",
    "singular"=>"order",
    "collect"=>"orders",
    "permission"=>[
        "order-full-access"=>"order full access",
        "order-list"=>"orders list",
        "order-delete"=>"order delete",
        "order-create"=>"order create",
        "order-edit"=>"edit order",
    ]


];
