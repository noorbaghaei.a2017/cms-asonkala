<li>
        <a href="{{route('employers.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('client.icons.employer')}}"></i>
                              </span>
            <span class="nav-text">{{__('client::employers.collect')}}</span>
        </a>
</li>
