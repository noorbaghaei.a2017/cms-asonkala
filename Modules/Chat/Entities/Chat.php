<?php

namespace Modules\Chat\Entities;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table='client_chats';
    protected $fillable = ['user','client','text','status'];
}
