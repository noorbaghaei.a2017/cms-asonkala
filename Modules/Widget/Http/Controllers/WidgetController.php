<?php

namespace Modules\Widget\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Widget\Entities\Widget;
use Modules\Widget\Http\Requests\WidgetRequest;
use Modules\Widget\Transformers\WidgetCollection;

class WidgetController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;


//category

    protected $route_categories_index='widget::categories.index';
    protected $route_categories_create='widget::categories.create';
    protected $route_categories_edit='widget::categories.edit';
    protected $route_categories='widget.categories';


//question

    protected $route_questions_index='widget::questions.index';
    protected $route_questions_create='widget::questions.create';
    protected $route_questions_edit='widget::questions.edit';
    protected $route_questions='widgets.index';


//notification

    protected $notification_store='widget::widgets.store';
    protected $notification_update='widget::widgets.update';
    protected $notification_delete='widget::widgets.delete';
    protected $notification_error='widget::widgets.error';



    public function __construct()
    {
        $this->entity=new Widget();
        $this->class=Widget::class;
        $this->middleware('permission:widget-list');
        $this->middleware('permission:widget-create')->only(['create','store']);
        $this->middleware('permission:widget-edit' )->only(['edit','update']);
        $this->middleware('permission:widget-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        try {
            $items=$this->entity->get();

            $widgets = new WidgetCollection($items);

            $data= collect($widgets->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Widget::class)->get();
            return view('widget::widgets.create',compact('categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('widget::widgets.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('widget::widgets.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(WidgetRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->order=$request->input('order');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->href=$request->input('href');
            $this->entity->status=$request->input('status');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if($request->has('video')){
                $this->entity->addMedia($request->file('video'))->toMediaCollection(config('cms.collection-video'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('widget::widgets.error'));
            }else{
                return redirect(route("widgets.index"))->with('message',__('widget::widgets.store'));
            }
        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('widget::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Widget::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('widget::widgets.edit',compact('item','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(WidgetRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "order"=>$request->input('order'),
                "href"=>$request->input('href'),
                "status"=>$request->input('status'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('video')){
                destroyMedia($this->entity,config('cms.collection-video'));
                $this->entity->addMedia($request->file('video'))->toMediaCollection(config('cms.collection-video'));
            }


            if(!$updated){
                return redirect()->back()->with('error',__('widget::widgets.error'));
            }else{
                return redirect(route("widgets.index"))->with('message',__('widget::widgets.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            if($this->entity->Hasmedia(config('cms.collection-video'))){
                destroyMedia($this->entity,config('cms.collection-video'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('widget::widgets.error'));
            }else{
                return redirect(route("widgets.index"))->with('message',__('widget::widgets.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
