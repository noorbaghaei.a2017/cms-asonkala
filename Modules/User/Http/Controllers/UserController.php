<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Modules\User\Http\Requests\PasswordUpdateUser;
use Modules\User\Http\Requests\InfoUpdateUser;
use Modules\Core\Entities\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Modules\Core\Entities\Branch;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Modules\User\Http\Requests\UserRequest;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    protected $entity,$manager;

    public function __construct()
    {
        $this->entity=new User();

        $this->middleware('permission:user-list')->only(['index']);
        $this->middleware('permission:user-create')->only(['create','store']);
        $this->middleware('permission:user-edit' )->only(['edit','update']);
        $this->middleware('permission:user-delete')->only(['destroy']);

       
    }
    use  ResetsPasswords;

    public function resetPass($object,$pass){

            $this->resetPassword($object,$pass);
    }
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            $data=[
                'auth'=>\auth('web')->user()
            ];
           

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $roles = Role::pluck('name','name')->all();
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('user::users.index',compact('items','roles'));
        }catch (\Exception $exception){
           
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            Artisan::call('cache:clear');
            $branches=Branch::latest()->get();
          
            return view('user::users.create',compact('roles','branches'));
        }catch (\Exception $exception){
           
            
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            $roles = Role::pluck('name','name')->all();
            if(
                !isset($request->email) &&
                !isset($request->mobile) &&
                !isset($request->roles)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('user::users.index',compact('items'));
            }

            $items=$this->entity
                ->where("email",trim($request->email))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(config('cms.paginate'));
            return view('user::users.index',compact('items','request','roles'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        try {

            $roles = Role::select('id')->whereName('manager')->first();
            $this->entity->first_name=$request->input('first_name');
            $this->entity->last_name=$request->input('last_name');
            $this->entity->email=$request->input('email');
            $this->entity->branch_access=$request->input('branch_access');
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->token=tokenGenerate();

            $this->entity->save();

            $this->entity->info()->create();

            $this->entity->company()->create();

            $this->entity->assignRole($roles->id);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            return redirect()->route('users.index')->with('message',__('user::users.update'));
        }catch (Exception $exception){
           
           
            return redirect()->back()->with('error',__('user::users.error'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(UserRequest $request,$token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($this->entity,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }

            $this->entity->update([
                'username'=>$request->input('username'),
                'first_name'=>$request->input('firstname'),
                'last_name'=>$request->input('lastname'),
                'mobile'=>checkZeroFirst($request->input('mobile')),
                'email'=>$request->input('email'),
            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,
            ]);

            $this->entity->company()->update([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,
            ]);


            $this->entity->syncRoles($request->input('roles'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            return redirect(route("users.index"))->with('message',__('user::users.update'));
        }catch (Exception $exception){
            
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return void
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $roles = Role::pluck('name','name')->all();
            $userRole = $item->roles->pluck('name','name')->all();
            return view('user::users.edit',compact('item','roles','userRole'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {

            $item=$this->entity->whereToken($token)->firstOrFail();
            if($item->Hasmedia(config('cms.collection-image'))){
                destroyMedia($item,config('cms.collection-image'));
            }
            $item->info()->delete();
            $deleted=$item->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('user::users.error'));
            }else{
                return redirect(route("users.index"))->with('message',__('user::users.delete'));
            }



        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    public  function profile(){
        try {

            $item=auth('web')->user();
            return view('core::dashboard.profile',compact('item'));
        }catch (\Exception $exception){

        
            return abort('500');
        }
    }
    public  function  password(){
        try {

            $item=auth('web')->user();
            return view('core::dashboard.profile',compact('item'));
        }catch (\Exception $exception){

        
            return abort('500');
        }
    }

   


    public function updateProfile(InfoUpdateUser $request,$token){
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $this->entity->update([
                "username"=>$request->input('username'),
                "firstname"=>$request->input('firstname'),
                "mobile"=>$request->input('mobile'),
                "email"=>$request->input('email'),
                "lastname"=>$request->input('lastname')
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection('images');
            }
           


            return redirect()->back()->with('message',__('user::users.update'));
        }catch (Exception $exception){
          
            return redirect()->back()->with('message',__('user::users.error'));
        }
    }


    public function updatePassword(PasswordUpdateUser $request,$token){

        try{
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if(!empty(trim($request->input('new_password')))){
                if($this->resetPasswordProfile($this->entity,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('message',__('user::users.update'));
                }
                else{
                    return redirect()->back()->with('error',__('user::users.error'));
                }
            }


        }catch(Exception $exception){
            return abort('500');
        }

    }

    public function updateUser(Request $request,$token){
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $this->entity->update([
                "username"=>$request->input('username'),
                "first_name"=>$request->input('firstname'),
                "mobile"=>checkZeroFirst($request->input('mobile')),
                "last_name"=>$request->input('lastname')
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!empty(trim($request->input('password')))){
                $this->resetPass($this->entity,$request->input('password'));
            }
            return redirect(route("users.index"))->with('message',__('user::users.update'));
        }catch (Exception $exception){
           
            return abort('500');
        }
    }
}
