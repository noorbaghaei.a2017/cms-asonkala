<?php

namespace Modules\Video\Database\Seeders;

use Illuminate\Database\Seeder;


class VideoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class
        ]);
    }
}
