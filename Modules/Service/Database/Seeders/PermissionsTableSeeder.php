<?php

namespace Modules\Service\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Service\Entities\Advantage;
use Modules\Service\Entities\Property;
use Modules\Service\Entities\Service;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Service::class)->delete();
        DB::table('permissions')->whereModel(Advantage::class)->delete();
        DB::table('permissions')->whereModel(Property::class)->delete();


        Permission::create(['name'=>'service-list','model'=>Service::class,'created_at'=>now()]);
        Permission::create(['name'=>'service-create','model'=>Service::class,'created_at'=>now()]);
        Permission::create(['name'=>'service-edit','model'=>Service::class,'created_at'=>now()]);
        Permission::create(['name'=>'service-delete','model'=>Service::class,'created_at'=>now()]);


        Permission::create(['name'=>'advantage-list','model'=>Advantage::class,'created_at'=>now()]);
        Permission::create(['name'=>'advantage-create','model'=>Advantage::class,'created_at'=>now()]);
        Permission::create(['name'=>'advantage-edit','model'=>Advantage::class,'created_at'=>now()]);
        Permission::create(['name'=>'advantage-delete','model'=>Advantage::class,'created_at'=>now()]);


        Permission::create(['name'=>'property-list','model'=>Property::class,'created_at'=>now()]);
        Permission::create(['name'=>'property-create','model'=>Property::class,'created_at'=>now()]);
        Permission::create(['name'=>'property-edit','model'=>Property::class,'created_at'=>now()]);
        Permission::create(['name'=>'property-delete','model'=>Property::class,'created_at'=>now()]);
    }
}
