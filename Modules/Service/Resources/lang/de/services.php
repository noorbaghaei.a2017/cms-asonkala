<?php
return [
    "text-create"=>"you can create your Services",
    "text-edit"=>"you can edit your Services",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"Services list",
    "singular"=>"Services",
    "collect"=>"Services",
    "permission"=>[
        "service-full-access"=>"Services full access",
        "service-list"=>"Services list",
        "service-delete"=>"Services delete",
        "service-create"=>"Services create",
        "service-edit"=>"edit Services",
    ]
];
