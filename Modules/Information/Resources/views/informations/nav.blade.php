<li>
        <a href="{{route('informations.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="material-icons">{{config('information.icons.information')}}</i>
                              </span>
            <span class="nav-text">{{__('information::informations.collect')}}</span>
        </a>
</li>
