<?php

namespace Modules\Educational\Helper;


class ClassRoomHelper{

    public static function status($status){
        switch ($status){
            case 1 :
                return '<span class="alert-success">فعال</span>';
                break;
            case 0 :
                return '<span class="alert-danger">غیر فعال</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function statusValue($status)
    {
        switch ($status) {
            case 1 :
                return 'فعال';
                break;
            case 0 :
                return ' غیر فعال';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }

}
