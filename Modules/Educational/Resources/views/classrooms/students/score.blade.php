@extends('core::layout.panel')
@section('pageTitle', __('cms.score'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.full_name')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->full_name}}</h4>
                            </div>
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.title')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$data->title}}</h4>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">

                            <small>
                                {{__('educational::students.score-register')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{hasChild($data,'classroom') ? route('student.classroom.register.score.multi', ['data'=>$data->token,'token' => $item->token]) : route('student.classroom.register.score', ['data'=>$data->token,'token' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            @csrf

                          @if(hasChild($data,'classroom'))

                              @foreach(getChild($data,'classroom') as $child)

                                    <div class="box" style="padding: 5px">
                                      <div class="form-group row">
                                          <div class="col-sm-12">
                                              <label  class="form-control-label">{{$child->title}}  </label>

                                          </div>
                                      </div>

                                      <div class="form-group row">
                                          <div class="col-sm-3">
                                              <span class="text-danger">*</span>
                                              <label for="score" class="form-control-label">{{__('cms.score')}}  </label>
                                              <input type="number" class="form-control" {{findAction($item,$child,'classroom')->score}}  id="score" name="{{$child->token}}_score" >

                                          </div>
                                          <div class="col-sm-3">
                                              <label for="rate" class="form-control-label">{{__('cms.rate')}}  </label>
                                              <input type="number"  class="form-control" {{findAction($item,$child,'classroom')->rate}} id="rate" name="{{$child->token}}_rate" >

                                          </div>
                                          <div class="col-sm-3">
                                              <label for="record" class="form-control-label">{{__('cms.record')}}  </label>
                                              <input type="number"  class="form-control" {{findAction($item,$child,'classroom')->record}} id="record" name="{{$child->token}}_record" >

                                          </div>
                                          <div class="col-sm-3">
                                              <label for="award" class="form-control-label">{{__('cms.awards')}}  </label>
                                              <select dir="rtl" class="form-control" id="award" name="{{$child->token}}_award" >
                                                  @foreach($awards as $award)
                                                      <option  value="{{$award->id}}" {{findAction($item,$child,'classroom')->award==$award->id ? "selected" : ""}}>{{$award->symbol}}</option>
                                                  @endforeach

                                              </select>
                                          </div>
                                      </div>
                                  </div>

                                @endforeach

                            @else

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label  class="form-control-label">{{$data->title}}  </label>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <span class="text-danger">*</span>
                                        <label for="score" class="form-control-label">{{__('cms.score')}}  </label>
                                        <input type="number" class="form-control" value="{{findAction($item,$data,'classroom')->score}}" id="score" name="score" >

                                    </div>
                                    <div class="col-sm-3">
                                        <label for="rate" class="form-control-label">{{__('cms.rate')}}  </label>
                                        <input type="number"  class="form-control" value="{{findAction($item,$data,'classroom')->rate}}" id="rate" name="rate" >

                                    </div>
                                    <div class="col-sm-3">
                                        <label for="record" class="form-control-label">{{__('cms.record')}}  </label>
                                        <input type="number"  class="form-control" value="{{findAction($item,$data,'classroom')->record}}" id="record" name="record" >

                                    </div>
                                    <div class="col-sm-3">
                                        <label for="award" class="form-control-label">{{__('cms.awards')}}  </label>
                                        <select dir="rtl" class="form-control" id="award" name="award" >
                                            @foreach($awards as $award)
                                                <option  value="{{$award->id}}" {{findAction($item,$data,'classroom')->award==$award->id ? "selected" : ""}}>{{$award->symbol}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            @endif



                            @include('core::layout.create-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {

            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                },
                messages: {
                    title:"عنوان الزامی است",

                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
