@include('core::layout.modules.leader.edit',[

    'title'=>__('core::leaders.create'),
    'item'=>$item,
    'parent'=>'educational',
    'model'=>'race',
    'directory'=>'races',
    'collect'=>__('core::leaders.collect'),
    'singular'=>__('core::leaders.singular'),
   'update_route'=>['name'=>'race.leader.update','param'=>'leader'],

])








