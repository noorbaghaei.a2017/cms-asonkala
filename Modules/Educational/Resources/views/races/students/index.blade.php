@include('core::layout.modules.index',[

    'title'=>__('educational::students.index'),
    'items'=>$items,
      'data'=>$data,
    'parent'=>'educational',
    'model'=>'race',
    'directory'=>'races',
       'collect'=>__('educational::students.collect'),
    'singular'=>__('educational::students.singular'),
        'destroy_route'=>['name'=>'destroy.race.client.force','name_param'=>'client','two_param'=>'classroom'],
      'score_route'=>true,
        'pagination'=>true,
       'add_student_route'=>true,
     'add_student_route_classroom'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
        __('cms.last_name')=>'last_name',
                __('cms.score')=>'score',
        __('cms.rate')=>'rate',
        __('cms.mobile')=>'mobile',
       __('cms.username')=>'username',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
        __('cms.last_name')=>'last_name',
            __('cms.score')=>'score',
        __('cms.rate')=>'rate',
      __('cms.username')=>'username',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
