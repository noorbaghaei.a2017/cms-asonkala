<?php

namespace Modules\Educational\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ClassRoomResource extends Resource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'thumbnail'=>(!$this->Hasmedia('images'))  ? asset('img/no-img.gif') : $this->getFirstMediaUrl('images', 'medium') ,
            'title'=>$this->title,
            'slug'=>$this->slug,
            'status'=>$this->showStatus,
            'price'=>$this->PriceFormat,
            'category'=>$this->NameCategory,
            'count_student'=>$this->CountStudent,
            'count_child'=>$this->CountChild,
            'view'=>$this->view,
            'like'=>$this->like,
            'questions'=>$this->question,
            'token'=>$this->token,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
        ];
    }
}
