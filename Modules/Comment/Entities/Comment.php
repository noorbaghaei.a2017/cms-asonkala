<?php

namespace Modules\Comment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Comment\Helper\CommentHelper;

class Comment extends Model
{
     use TimeAttribute;

     public function getShowStatusAttribute(){

        return CommentHelper::ShowStatusStyle($this->status);
    }

    protected $table="cms_comments";

    protected $fillable = ['title','token','email','client','user','secret','text','status','commentable_id','commentable_type','status'];



    public function commentable()
    {
        return $this->morphTo();
    }
}
