<?php

namespace Modules\Event\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\User;
use Modules\Core\Entities\Week;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Event extends Model implements HasMedia
{
    use HasMediaTrait,Sluggable,HasTags,TimeAttribute;

    protected $fillable = ['title','category','text','excerpt','token','slug','user','order','country','google_map','sign_start_at','sign_end_at','longitude','latitude','address','refer_link','short_link'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function week()
    {
        return $this->morphOne(Week::class, 'weekable');
    }

    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }


    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }
    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(220)
            ->height(115)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
