<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientMenuJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_menu_job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client');
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('menu');
            $table->foreign('menu')->references('id')->on('menu_job')->onDelete('cascade');
            $table->unsignedBigInteger('service');
            $table->foreign('service')->references('id')->on('user_services')->onDelete('cascade');
            $table->string('price')->nullable();
            $table->string('title');
            $table->string('excerpt')->nullable();
            $table->text('text')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_menu_job');
    }
}
