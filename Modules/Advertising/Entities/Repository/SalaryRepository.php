<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Salary;

class SalaryRepository implements SalaryRepositoryInterface
{

    public function getAll()
    {
       return Salary::latest()->get();
    }
}
