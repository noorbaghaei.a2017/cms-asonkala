<?php

namespace Modules\Advertising\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Advertising\Entities\Period;

class PeriodCollection extends ResourceCollection
{
    public $collects=Period::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new PeriodResource($item);
                }
            ),
            'filed' => [
                'title','symbol','slug','order','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'periods.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'periods.edit',
                        'param'=>[
                            'period'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'periods.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.period',
                'filter'=>[
                    'title','symbol','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('advertising::periods.collect'),
            'title'=>__('advertising::periods.index'),
        ];
    }
}
