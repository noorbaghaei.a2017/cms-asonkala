<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/advertisings', 'AdvertisingController')->only('create','store','update','index','edit');
    Route::resource('/jobs', 'JobController')->only('create','store','update','index','edit');
    Route::resource('/categoryjobs', 'CategoryJobController')->only('create','store','update','index','edit');   
    Route::post('job/import','JobController@importFile')->name('job.import');
    Route::post('category/job/import','CategoryJobController@importFile')->name('categoryjob.import');
    Route::post('menu/job/import','MenuJobController@importFile')->name('menujob.import');
    Route::post('attribute/job/import','AttributeJobController@importFile')->name('attributejob.import');
    Route::get('job/{lang}/{token}', 'JobController@languageShow')->name('job.language.show');
    Route::patch('job/lang/update/{lang}/{token}', 'JobController@languageUpdate')->name('job.language.update');
    Route::resource('/menujobs', 'MenuJobController')->only('create','store','update','index','edit');      
    Route::resource('/attributejobs', 'AttributeJobController')->only('create','store','update','index','edit');      
    Route::get('categoryjobs/{lang}/{token}', 'CategoryJobController@languageShow')->name('categoryjob.language.show');
    Route::get('menujobs/{lang}/{token}', 'MenuJobController@languageShow')->name('menujob.language.show');
    Route::get('attributejobs/{lang}/{token}', 'AttributeJobController@languageShow')->name('attributejob.language.show');  
    Route::post('/jobs/approved/{id}', 'JobController@approved')->name('job.approved');
    Route::patch('categoryjobs/lang/update/{lang}/{token}', 'CategoryJobController@languageUpdate')->name('categoryjob.language.update');
    Route::patch('menujobs/lang/update/{lang}/{token}', 'MenuJobController@languageUpdate')->name('menujob.language.update');
    Route::patch('attributejobs/lang/update/{lang}/{token}', 'AttributeJobController@languageUpdate')->name('attributejob.language.update');
    Route::resource('/skills', 'SkillController')->only('create','store','destroy','update','index','edit');
    Route::resource('/positions', 'PositionController')->only('create','store','destroy','update','index','edit');
    Route::resource('/salaries', 'SalaryController')->only('create','store','destroy','update','index','edit');
    Route::resource('/types', 'TypeController')->only('create','store','destroy','update','index','edit');
    Route::resource('/periods', 'PeriodController')->only('create','store','destroy','update','index','edit');
    Route::resource('/experiences', 'ExperienceController')->only('create','store','destroy','update','index','edit');
    Route::resource('/educations', 'EducationController')->only('create','store','destroy','update','index','edit');
    Route::resource('/guilds', 'GuildController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'advertisings'], function () {
        Route::get('/gallery/{advertising}', 'AdvertisingController@gallery')->name('advertising.gallery');
        Route::post('/gallery/store/{advertising}', 'AdvertisingController@galleryStore')->name('advertising.gallery.store');
        Route::get('/gallery/destroy/{media}', 'AdvertisingController@galleryDestroy')->name('advertising.gallery.destroy');
    });

    Route::group(["prefix"=>'categoryjob'], function () {
        Route::get('/gallery/{categoryjob}', 'CategoryJobController@gallery')->name('category_jobs.gallery');
        Route::post('/gallery/store/{categoryjob}', 'CategoryJobController@galleryStore')->name('category_jobs.gallery.store');
        Route::get('/gallery/destroy/{media}', 'CategoryJobController@galleryDestroy')->name('category_jobs.gallery.destroy');
    });


    Route::group(["prefix"=>'search'], function () {
        Route::post('/advertisings', 'AdvertisingController@search')->name('search.advertising');
        Route::post('/skills', 'SkillController@search')->name('search.skill');
        Route::post('/positions', 'PositionController@search')->name('search.position');
        Route::post('/salaries', 'SalaryController@search')->name('search.salary');
        Route::post('/types', 'TypeController@search')->name('search.type');
        Route::post('/periods', 'PeriodController@search')->name('search.period');
        Route::post('/experiences', 'ExperienceController@search')->name('search.experience');
        Route::post('/educations', 'EducationController@search')->name('search.education');
        Route::post('/guilds', 'EducationController@search')->name('search.guild');
        Route::post('/categoryjobs', 'CategoryJobController@search')->name('search.categoryJob');

  
    });



    Route::group(["prefix"=>'advertising/categories'], function () {
        Route::get('/', 'AdvertisingController@categories')->name('advertising.categories');
        Route::get('/create', 'AdvertisingController@categoryCreate')->name('advertising.category.create');
        Route::post('/store', 'AdvertisingController@categoryStore')->name('advertising.category.store');
        Route::get('/edit/{category}', 'AdvertisingController@categoryEdit')->name('advertising.category.edit');
        Route::patch('/update/{category}', 'AdvertisingController@categoryUpdate')->name('advertising.category.update');

    });

    Route::group(["prefix"=>'advertising/questions'], function () {
        Route::get('/{advertising}', 'AdvertisingController@question')->name('advertising.questions');
        Route::get('/create/{advertising}', 'AdvertisingController@questionCreate')->name('advertising.question.create');
        Route::post('/store/{advertising}', 'AdvertisingController@questionStore')->name('advertising.question.store');
        Route::delete('/destroy/{question}', 'AdvertisingController@questionDestroy')->name('advertising.question.destroy');
        Route::get('/edit/{advertising}/{question}', 'AdvertisingController@questionEdit')->name('advertising.question.edit');
        Route::patch('/update/{question}', 'AdvertisingController@questionUpdate')->name('advertising.question.update');
    });

});
