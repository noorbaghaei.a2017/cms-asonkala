<?php


namespace Modules\Plan\Entities\Repository;


interface PlanRepositoryInterface
{

    public function getAll();
}
