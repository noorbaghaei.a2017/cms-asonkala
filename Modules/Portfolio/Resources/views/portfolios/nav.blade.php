<li>
        <a href="{{route('portfolios.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('portfolio.icons.portfolio')}}"></i>
                              </span>
            <span class="nav-text">{{__('portfolio::portfolios.collect')}}</span>
        </a>
    </li>

