<?php


namespace Modules\Portfolio\Entities\Repository;


use Modules\Portfolio\Entities\Portfolio;

class PortfolioRepository implements PortfolioRepositoryInterface
{

    public function getAll()
    {
       return Portfolio::latest()->get();
    }
}
