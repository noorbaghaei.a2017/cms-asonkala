<?php

namespace Modules\Product\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Product\Entities\Cart;

class CheckAdressProduct
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!checkAddress(Cart::whereClient(auth('client')->user()->id)->first())){

            return redirect(route('client.show.shopping'));
        }

        return $next($request);
    }
}
