<?php

namespace Modules\Product\Transformers\OrderList;

use Illuminate\Http\Resources\Json\Resource;

class OrderListProductResource extends Resource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'=>$this->order_id,
            'title'=>$this->titlePayment,
            'full_name'=>$this->fullNameUser,
            'mobile'=>$this->mobileUser,
            'email'=>$this->emailUser,
            'price'=>$this->price,
            'status'=>$this->showStatus,
            'token'=>$this->token,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,

        ];
    }
}
