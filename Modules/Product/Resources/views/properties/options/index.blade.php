@include('core::layout.modules.index',[

    'title'=>__('core::options.index'),
    'items'=>$items,
    'parent'=>'product',
    'model'=>'product',
    'directory'=>'properties',
     'singular_value'=>'property',
    'popular_value'=>'properties',
    'collect'=>__('core::options.collect'),
    'singular'=>__('core::options.singular'),
   'create_route'=>['name'=>'property.option.create'],
    'edit_route'=>['name'=>'property.option.edit','name_param'=>'option'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
       __('cms.symbol')=>'symbol',
       __('cms.order')=>'order',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
                __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
       __('cms.symbol')=>'symbol',
          __('cms.order')=>'order',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



