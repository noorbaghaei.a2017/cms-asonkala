
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.search')}} </h2>

            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                @include('core::layout.alert-danger')
                <form role="form" method="post" action="{{route('search.product')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">

                        <div class="col-sm-3">
                            <label for="title" class="form-control-label">{{__('cms.title')}}  </label>
                            <input type="text" value="{{isset($request->title) ? $request->title : ""}}" name="title" class="form-control" id="last_name" >
                        </div>
                        <div class="col-sm-3">
                            <label for="price" class="form-control-label">{{__('cms.price')}}  </label>
                            <input type="text" value="{{isset($request->price) ?  $request->price :""}}" name="price" class="form-control" id="mobile" >
                        </div>

                    </div>

                    <div class="form-group row m-t-md">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

