<?php


namespace Modules\Product\Entities\Repository\Product;


use Modules\Product\Entities\Product;

class ProductRepository implements ProductRepositoryInterface
{

    public function getAll()
    {
       return Product::latest()->get();
    }
}
