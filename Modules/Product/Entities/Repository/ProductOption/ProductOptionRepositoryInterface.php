<?php


namespace Modules\Product\Entities\Repository\ProductOption;


interface ProductOptionRepositoryInterface
{
    public function getAll();
}
