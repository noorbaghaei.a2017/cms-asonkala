<?php

namespace Modules\Product\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Option;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class OptionProperty extends Model implements HasMedia
{
    use TimeAttribute,Sluggable,HasMediaTrait;

    protected $fillable = ['title','text','title','icon','user','token','icon','order','slug','excerpt'];

    public function options()
    {
        return $this->morphMany(Option::class, 'optionable');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
