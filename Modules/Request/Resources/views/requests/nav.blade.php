<li>
    <a href="{{route('requests.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('request.icons.request')}}"></i>
                              </span>
        <span class="nav-text">{{__('request::requests.collect')}}</span>
    </a>
</li>

