<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Page\Entities\Page;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Page::class)->delete();

        Permission::create(['name'=>'page-list','model'=>Page::class,'created_at'=>now()]);
        Permission::create(['name'=>'page-create','model'=>Page::class,'created_at'=>now()]);
        Permission::create(['name'=>'page-edit','model'=>Page::class,'created_at'=>now()]);
        Permission::create(['name'=>'page-delete','model'=>Page::class,'created_at'=>now()]);

    }
}
