<?php


namespace Modules\Core\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Advertising\Entities\Advertising;
use Modules\Article\Entities\Article;
use Modules\Brand\Entities\Brand;
use Modules\Carousel\Entities\Carousel;
use Modules\Core\Entities\Category;
use Modules\Customer\Entities\Customer;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Question\Entities\Question;
use Modules\Service\Entities\Service;
use Modules\Store\Entities\Store;

class CoreHelper
{
    public static function checkSubCategory(Model $category,$token){

        if($category->parent==0){
            return "";
        }
        else{
            if(Category::whereId($category->parent)->first()->token==$token){
                return "selected";
            }
            return "";
        }

    }

    public static function hasSlider(){

        if(Carousel::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasProduct(){

        if(Product::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasPlan(){

        if(Plan::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasEvent(){

        if(Event::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasQuestion(){

        if(Question::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasStore(){

        if(Store::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasMember(){

        if(Member::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasBrand(){

        if(Brand::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasCustomer(){

        if(Customer::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasCourse(){

        if(ClassRoom::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasPortfolio(){

        if(Portfolio::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasInformation(){

        if(Information::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasAdvertising(){

        if(Advertising::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasRace(){

        if(Race::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasService(){

        if(Service::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasArticle(){

        if(Article::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
}
