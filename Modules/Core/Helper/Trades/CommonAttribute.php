<?php


namespace Modules\Core\Helper\Trades;


trait CommonAttribute
{

    public  function getGetStatusAttribute(){

        switch ($this->status) {

            case '0' :
                return "<span class='alert-danger'>غیر فعال</span>";
            case '1' :
                return "<span class='alert-success'> فعال</span>";
        }
    }

}
