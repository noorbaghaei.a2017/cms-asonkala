<?php
return [
    "text-create"=>"you can create your category",
    "text-edit"=>"you can edit your category",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"products list",
    "singular"=>"category",
    "collect"=>"categories",
    "permission"=>[
        "category-full-access"=>"categories full access",
        "category-list"=>"categories list",
        "category-delete"=>"category delete",
        "category-create"=>"category create",
        "category-edit"=>"edit category",
    ]
];
