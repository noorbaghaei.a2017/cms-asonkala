<?php
return [
    "text-create"=>"you can create your report",
    "text-edit"=>"you can edit your report",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"reports list",
    "error"=>"error",
    "singular"=>"report",
    "collect"=>"reports",
    "permission"=>[
        "report-full-access"=>"report full access",
        "report-list"=>"reports list",
        "report-delete"=>"report delete",
        "report-create"=>"report create",
        "report-edit"=>"edit report",
    ]


];
