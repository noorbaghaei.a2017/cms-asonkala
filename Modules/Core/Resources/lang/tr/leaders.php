<?php
return [
    "text-create"=>"you can create your leader",
    "text-edit"=>"you can edit your leader",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"products list",
    "singular"=>"leader",
    "collect"=>"leaders",
    "permission"=>[
        "leader-full-access"=>"leaders full access",
        "leader-list"=>"leaders list",
        "leader-delete"=>"leader delete",
        "leader-create"=>"leader create",
        "leader-edit"=>"edit leader",
    ]
];
