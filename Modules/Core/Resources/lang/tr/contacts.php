<?php
return [
    "store"=>"Store Success",
    "send"=>"Send Contact",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"contacts list",
    "singular"=>"contact",
    "collect"=>"contact",
    "permission"=>[
        "brand-full-access"=>"contact full access",
        "brand-list"=>"contacts list",
        "brand-delete"=>"contact delete",
    ]
];
