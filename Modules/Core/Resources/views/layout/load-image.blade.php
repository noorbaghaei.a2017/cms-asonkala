@if(!$item->Hasmedia($type) && statusUrl(findAvatar($item->email,200))===200)
    <a target="_blank" href="{{findAvatar($item->email,200)}}"><img style="width:80px;height: auto" src="{{findAvatar($item->email,200)}}" alt="" class="img-responsive" ></a>

@elseif(!$item->Hasmedia($type) && statusUrl(findAvatar($item->email,200))!==200)

    <a target="_blank" href="{{asset('img/no-img.gif')}}"><img style="width: 80px;height: auto" src="{{asset('img/no-img.gif')}}" class="img-responsive" ></a>
@else

    <a target="_blank" href="{{$item->getFirstMediaUrl($type)}}"><img style="width: 80px;height: auto" src="{{$item->getFirstMediaUrl($type)}}" alt="" class="img-responsive" ></a>

@endif
