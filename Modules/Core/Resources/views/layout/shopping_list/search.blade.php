
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h2>{{__('cms.search')}} </h2>
                      <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white">{{__('cms.print')}} </a>

                </div>
                <div class="box-divider m-a-0"></div>
                <div class="box-body">
                    @include('core::layout.alert-danger')
                    <form role="form" method="post" action="{{route('search.shopping.list')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            
                            <div class="col-sm-3">
                                <label for="text" class="form-control-label">{{__('cms.text')}}  </label>
                                <input type="text" value="{{isset($request->text) ? $request->text :"" }}" name="text" class="form-control" id="text" autocomplete="off">
                            </div>
                            <div class="col-sm-3">
                                <label for="branch" class="form-control-label">{{__('cms.branch')}}  </label>
                                <select name="branch" class="form-control">
                                @foreach($branches as $branch)
                                
                                @if(isset($request->branch))
                                <option value="{{$branch->id}}" {{($request->branch==$branch->id) ? 'selected' : ''}}>{{$branch->title}} </option>
                                @else
                                <option value="{{$branch->id}}" >{{$branch->title}} </option>
                                @endif
                                @endforeach
                                </select>
                            </div>
                            

                        </div>

                        <div class="form-group row m-t-md">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                            </div>
                        </div>
                    </form>
                  
                </div>
               

            </div>
        </div>
    </div>

