@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$branch_item->title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">Rathaus galerie Leverkusen</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
                        <a href="{{route('create.accounting.list.branch',['branch'=>$branch_item->id])}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
                       
                        <br>
<br>

<h3 class="card-title">Daten </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="7">{{$branch_item->title}}</th></tr>

                      <tr>
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.glas')}}</th>
                        <th>{{__('cms.tasche')}}</th>
                        <th>{{__('cms.hulle')}}</th>
                        <th>{{__('cms.zubehhor')}}</th>
                        <th>{{__('cms.option')}}</th>

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $item)
                  <tr>
                     
                          
                    
                  <td>{{(!is_null($item->member)) ? getInfoMember($item->member)->email : 'Admin'}}</td>
                    <td>{{($item->created_at)->addHour(2)}}</td>
                    <td>{!! $item->glas !!}</td>
                    <td>{!!$item->tasche !!}</td>
                    <td>{!! $item->hulle !!}</td>
                    <td>{!! $item->zubehhor !!}</td>
                    <td>
                    @if($item->status!="2")
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-lg">
                      <i class="fa fa-times"></i>
                </button>
                <form action="{{route('shopping.list.deative',['report'=>$item->id,'branch'=>$branch_item->id])}}" method="POST" class="modal fade" id="modal-lg">
@csrf       
<div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">{{__('cms.note')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>{{__('cms.are-you-sure')}}</p>
              <input type="text" name="price_mode" autocomplete="off" placeholder="Produktkosten (ZB: 1445.65)">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{__('cms.no')}}</button>
              <input type="submit" value="{{__('cms.yes')}}" class="btn btn-primary">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</form>
                @endif
                      </td>
                    

                  
                  </tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.glas')}}</th>
                        <th>{{__('cms.tasche')}}</th>
                        <th>{{__('cms.hulle')}}</th>
                        <th>{{__('cms.zubehhor')}}</th>
                        <th>{{__('cms.option')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>


$('input[name=price_mode]').keyup(function(){
   
   $(this).val($(this).val().replace(/[^\d.]/,''));
});


  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection


@section('pageTitle')

{{$branch_item->title}}

@endsection








