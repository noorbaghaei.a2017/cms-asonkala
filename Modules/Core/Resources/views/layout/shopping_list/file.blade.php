@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>City Center Chorweiler</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">City Center Chorweiler</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

     
      <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">{{__('cms.data')}}</h4>
              </div>
              <div class="card-body">
                <div class="row">
         
                @foreach($items as $item)
                    @if($item->Hasmedia('images'))
                    <div class="col-sm-2">
                        <a target="_blank" href="{{$item->getFirstMediaUrl('images')}}" data-toggle="lightbox" data-title="sample 12 - black" data-gallery="gallery">
                        <img src="{{$item->getFirstMediaUrl('images')}}" class="img-fluid mb-2" alt="black sample"/>
                        </a>
                    </div>
        
                    @else
                    
                    @endif
                 
                  @endforeach

                </div>
              </div>
            </div>

     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')



@endsection

@section('pageTitle')

file

@endsection








