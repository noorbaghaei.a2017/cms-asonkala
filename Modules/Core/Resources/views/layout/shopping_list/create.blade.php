@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.report')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.report')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


               
                    <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/Default-welcomer.png')}}"
                       alt="" style="border-radius:unset;width:100%;">

    
                 


                </div>

                <h3 class="profile-username text-center">{{old('title')}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.about-us')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('store.accounting.list.branch',['branch'=>$branch_item->id])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                     
                    
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.glas')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="glas" value="{{old('glas')}}" class="form-control" id="glas" placeholder="{{__('cms.glas')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="tasche" class="col-sm-2 col-form-label">{{__('cms.tasche')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="tasche" value="{{old('tasche')}}" class="form-control" id="tasche" placeholder="{{__('cms.tasche')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="zubehhor" class="col-sm-2 col-form-label">{{__('cms.zubehhor')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="zubehhor" value="{{old('zubehhor')}}" class="form-control" id="zubehhor" placeholder="{{__('cms.zubehhor')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="hulle" class="col-sm-2 col-form-label">{{__('cms.hulle')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="hulle" value="{{old('hulle')}}" class="form-control" id="hulle" placeholder="{{__('cms.hulle')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="kasse" class="col-sm-2 col-form-label">{{__('cms.kasse')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="kasse" value="{{old('kasse')}}" class="form-control" id="kasse" placeholder="{{__('cms.kasse')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="karte" class="col-sm-2 col-form-label">{{__('cms.karte')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="karte" value="{{old('karte')}}" class="form-control" id="karte" placeholder="{{__('cms.karte')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="bar" class="col-sm-2 col-form-label">{{__('cms.bar')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="bar" value="{{old('bar')}}" class="form-control" id="bar" placeholder="{{__('cms.bar')}}">
                        </div>
                      </div>


                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                  

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection
 
 
 














