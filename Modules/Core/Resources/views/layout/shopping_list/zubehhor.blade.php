@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Rein berg galerie Bergisch Gladbach</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">Rein berg galerie Bergisch Gladbach</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="6">Rein berg galerie Bergisch Gladbach</th></tr>

                      <tr>
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.glas')}}</th>
                        <th>{{__('cms.tasche')}}</th>
                        <th>{{__('cms.hulle')}}</th>
                        <th>{{__('cms.zubehhor')}}</th>

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $item)
                  <tr>
                     
                          
                    
                  <td>{{getInfoMember($item->member)->email}}</td>
                    <td>{{$item->created_at->addHour(2)}}</td>
                    <td>{{$item->glas}}</td>
                    <td>{{$item->tasche}}</td>
                    <td>{{$item->hulle}}</td>
                    <td>{{$item->zubehhor}}</td>

                  
                  </tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.glas')}}</th>
                        <th>{{__('cms.tasche')}}</th>
                        <th>{{__('cms.hulle')}}</th>
                        <th>{{__('cms.zubehhor')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection

@section('pageTitle')

Rein berg galerie Bergisch Gladbach

@endsection








