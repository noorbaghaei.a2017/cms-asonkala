@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">

      <h5 class="mb-2">{{__('cms.to_now')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.karte')}}</span>
                @if($calc_karte['euro']!='0' && $calc_karte['cent']!='0')
                <span class="info-box-number">{{number_format($calc_karte['euro'])}} Euro {{$calc_karte['cent']}} Cent</span>

                @elseif($calc_karte['cent']=='0')

                <span class="info-box-number">{{number_format($calc_karte['euro'])}} Euro</span>
                @elseif($calc_karte['euro']=='0')
                <span class="info-box-number">{{$calc_karte['cent']}} Cent</span>
               
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.kasse')}}</span>
                @if($calc_kasse['euro']!='0' && $calc_kasse['cent']!='0')
                <span class="info-box-number">{{number_format($calc_kasse['euro'])}} Euro {{$calc_kasse['cent']}} Cent</span>

                @elseif($calc_kasse['cent']=='0')

                <span class="info-box-number">{{number_format($calc_kasse['euro'])}} Euro</span>
                @elseif($calc_kasse['euro']=='0')
                <span class="info-box-number">{{$calc_kasse['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.bar')}}</span>
                @if($calc_bar['euro']!='0' && $calc_bar['cent']!='0')
                <span class="info-box-number">{{number_format($calc_bar['euro'])}} Euro {{$calc_bar['cent']}} Cent</span>

                @elseif($calc_bar['cent']=='0')

                <span class="info-box-number">{{number_format($calc_bar['euro'])}} Euro</span>
                @elseif($calc_bar['euro']=='0')
                <span class="info-box-number">{{$calc_bar['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        
        </div>

        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$branch_item->title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">Rathaus galerie Leverkusen</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
                <a href="{{ route('yesterday.accounting.list.branch',['branch'=>$branch_item->id]) }}" class="btn btn-info">Letzte Monate</a>
                <br>
                <br>
                <h3 class="card-title">Daten</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="6">{{$branch_item->title}}</th></tr>

                      <tr>
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.kasse')}}</th>
                        <th>{{__('cms.bar')}}</th>
                        <th>{{__('cms.karte')}}</th>
                      
                      
                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $item)
                  <tr>
                     
                          
                    
                  <td>{{(!is_null($item->member)) ? getInfoMember($item->member)->email : 'Admin'}}</td>
                    <td>{{($item->created_at)->addHour(2)}}</td>
                    <td>{{$item->kasse}}€</td>
                    <td>{{$item->bar}}€</td>
                    <td>{{$item->karte}}€</td>
                   

                  
                  </tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.kasse')}}</th>
                        <th>{{__('cms.bar')}}</th>
                        <th>{{__('cms.karte')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection


@section('pageTitle')

{{$branch_item->title}}

@endsection








