<div class="padding">
    <div class="row m-b">

        <!-- @can('article-list')
        @if(hasModule('Article'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3> {{__('cms.articles')}}</h3>

                </div>

                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                        <th>{{__('cms.title')}} </th>
                        <th>{{__('cms.excerpt')}}</th>
                        <th>{{__('cms.create_date')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastArticles as $article)
                    <tr>


                    <td>{{$article->title}} </td>
                    <td>{{$article->excerpt}} </td>
                    <td>{{$article->created_at->ago()}} </td>



                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @endcan

            @can('product-list')
            @if(hasModule('Product'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3>{{__('cms.products')}}  </h3>
                </div>
                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                        <th>{{__('cms.title')}} </th>
                        <th>{{__('cms.excerpt')}}</th>
                        <th>{{__('cms.create_date')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastProducts as $product)
                        <tr>


                            <td>{{$product->title}} </td>
                            <td>{{$product->excerpt}} </td>
                            <td>{{$product->created_at->ago()}} </td>



                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
                @endif
                @endcan -->


    </div>
    <div class="row m-b">

 


                @can('user-list')
        @if(hasModule('User'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3> {{__('user::users.collect')}}</h3>
                </div>
                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                      
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.mobile')}}</th>
                        <th>{{__('cms.role')}}</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastUsers as $user)
                        <tr>


                         
                            <td>{{$user->email}} </td>
                            <td>{{$user->mobile}} </td>
                            <td>{{__('cms.'.$user->RoleName)}} </td>
                           



                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        @endif

            @endcan


       
            

            @can('member-list')
                @if(hasModule('Member'))
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> {{__('member::members.collect')}}</h3>
                            </div>
                            <table class="table table-striped b-t">
                                <thead>
                                <tr>
                               
                                    <th>{{__('cms.email')}}</th>
                                    <th>{{__('cms.create_date')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastMembers as $member)
                                    <tr>


                                        <td>{{$member->email}} </td>
                                        <td>{{$member->created_at->ago()}} </td>



                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                @endif

            @endcan

            @can('member-list')
                @if(hasModule('Member'))
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> {{__('core::branches.collect')}}</h3>
                            </div>
                            <table class="table table-striped b-t">
                                <thead>
                                <tr>
                               
                                    <th>{{__('cms.title')}}</th>
                                  
                                
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($branches as $branch)
                                    <tr>


                                        <td>{{$branch->title}} </td>
                                       
                                      

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                @endif

            @endcan

            @can('member-list')
                @if(hasModule('Member'))
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> Online Mitglied</h3>
                            </div>
                            <table class="table table-striped b-t">
                                <thead>
                                <tr>
                               
                                    <th>{{__('cms.email')}}</th>
                                 
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($members_online as $online)
                                    <tr>


                                        <td>{{$online->id}} </td>
                                       



                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                @endif

            @endcan




            <!-- @can('client-list')
                @if(hasModule('Client'))
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> {{__('client::clients.collect')}}</h3>
                            </div>
                            <table class="table table-striped b-t">
                                <thead>
                                <tr>
                                    <th>{{__('cms.full_name')}} </th>
                                    <th>{{__('cms.email')}}</th>
                                    <th>{{__('cms.username')}}</th>
                                    <th>{{__('cms.mobile')}}</th>
                                    <th>{{__('cms.role')}}</th>
                                    <th>{{__('cms.create_date')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastClients as $client)
                                    <tr>
                                        <td>{{fullName($client->first_name,$client->last_name)}} </td>
                                        <td>{{$client->email}} </td>
                                        <td>{{$client->username}} </td>
                                        <td>{{$client->mobile}} </td>
                                        <td>{{__($client->RoleName)}} </td>
                                        <td>{{$client->created_at->ago()}} </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                @endif

            @endcan -->

    </div>
</div>
