<div class="padding">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.holiday-today')}} <b style="color:red">{{\Morilog\Jalali\Jalalian::forge('today')->format('%A, %d %B %Y')}}</b> </h2>

            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                <ul>
                   @if(isset(get_info_today()->events))
                    @foreach(get_info_today()->events as $holiday)
                            @if($holiday->isHoliday)
                        <li style="color: red !important;">{{$holiday->event}}</li>
                            @endif
                    @endforeach
                    @else
                       @if(get_info_today()->isHoliday)
                            @if(isset(get_info_today()->event))
                                <li style="color: red !important;">{{get_info_today()->event}}</li>
                            @endif

                        @endif
                    @endif



                </ul>
            </div>
        </div>
    </div>
</div>
</div>

