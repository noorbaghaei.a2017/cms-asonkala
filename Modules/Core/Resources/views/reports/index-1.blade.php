@include('core::layout.modules.index',[

    'title'=>__('core::reports.index'),
    'items'=>$items,
    'parent'=>'core',
    'model'=>'memberreport',
    'directory'=>'reports',
    'collect'=>__('core::reports.collect'),
    'singular'=>__('core::reports.singular'),
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
            __('cms.email')=>'Email',
    __('cms.start')=>'start',
    __('cms.end')=>'end',
    __('cms.result')=>'Result',
    __('cms.break')=>'get_break',
    __('cms.start_location')=>'start_location',
    __('cms.end_location')=>'end_location',
    __('cms.text')=>'text',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
            __('cms.email')=>'Email',
            __('cms.start')=>'start',
    __('cms.end')=>'end',
    __('cms.break')=>'get_break',
    __('cms.result')=>'Result',
    __('cms.start_location')=>'start_location',
    __('cms.end_location')=>'end_location',

    __('cms.text')=>'text',

    ],
])
