@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
        
          
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{$branch_item->title}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">

              <a href="{{ route('yesterday.report.list.branch',['branch'=>$branch_item->id]) }}" class="btn btn-info">Letzte Monate</a>
              <br>
              <br>
                <h3 class="card-title">Daten </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="7">{{$branch_item->title}}</th></tr>

                      <tr>
                      <th>{{__('cms.date')}}</th>
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.start')}}</th>
                  <th>{{__('cms.end')}}</th>
                  <th>{{__('cms.today_time')}}</th>
                  <th>{{__('cms.start_location')}}</th>
                  <th>{{__('cms.end_location')}}</th>

                  
                       

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $item)
                  <tr>
                     
                          
                  <td>{{($item->created_at)->addHour(2)}}</td>
                  <td>{{(!is_null($item->email)) ? $item->email : 'Admin'}}</td>
                  <td>{{$item->start}}</td>
                  <td>{{$item->end}}</td>
                  <td>{{$item->today_time}}</td>
                  <td>{!!$item->start_location!!}</td>
                  <td>{!!$item->end_location!!}</td>
                   

                  
                  </tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>{{__('cms.date')}}</th>
                  <th>{{__('cms.email')}}</th>
                  <th>{{__('cms.start')}}</th>
                  <th>{{__('cms.end')}}</th>
                  <th>{{__('cms.today_time')}}</th>
                  <th>{{__('cms.start_location')}}</th>
                  <th>{{__('cms.end_location')}}</th>
                     
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection

@section('pageTitle')

{{$branch_item->title}}

@endsection








