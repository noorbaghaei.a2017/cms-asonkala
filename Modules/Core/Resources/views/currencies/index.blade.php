@include('core::layout.modules.index',[

    'title'=>__('core::currencies.index'),
    'items'=>$items,
    'parent'=>'core',
    'model'=>'currency',
    'directory'=>'currencies',
    'collect'=>__('core::currencies.collect'),
    'singular'=>__('core::currencies.singular'),
    'create_route'=>['name'=>'currencies.create'],
    'edit_route'=>['name'=>'currencies.edit','name_param'=>'currency'],
    'destroy_route'=>['name'=>'currencies.destroy','name_param'=>'currency'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.symbol')=>'symbol',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.symbol')=>'symbol',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
