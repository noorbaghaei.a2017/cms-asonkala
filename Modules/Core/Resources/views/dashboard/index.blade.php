@extends('core::dashboard.main')

@section('content')
   




    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{__('cms.dashboard')}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">Ganzjob</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

      <h5 class="mb-2">{{__('cms.today')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.karte')}}</span>
                @if($total_karte_today['euro']!='0' && $total_karte_today['cent']!='0')
                <span class="info-box-number">{{number_format($total_karte_today['euro'])}} Euro {{$total_karte_today['cent']}} Cent</span>

                @elseif($total_karte_today['cent']=='0')

                <span class="info-box-number">{{number_format($total_karte_today['euro'])}} Euro</span>
                @elseif($total_karte_today['euro']=='0')
                <span class="info-box-number">{{$total_karte_today['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.kasse')}}</span>
                @if($total_kasse_today['euro']!='0' && $total_kasse_today['cent']!='0')
                <span class="info-box-number">{{number_format($total_kasse_today['euro'])}} Euro {{$total_kasse_today['cent']}} Cent</span>

                @elseif($total_kasse_today['cent']=='0')

                <span class="info-box-number">{{number_format($total_kasse_today['euro'])}} Euro</span>
                @elseif($total_kasse_today['euro']=='0')
                <span class="info-box-number">{{$total_kasse_today['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.bar')}}</span>
                @if($total_bar_today['euro']!='0' && $total_bar_today['cent']!='0')
                <span class="info-box-number">{{number_format($total_bar_today['euro'])}} Euro {{$total_bar_today['cent']}} Cent</span>

                @elseif($total_bar_today['cent']=='0')

                <span class="info-box-number">{{number_format($total_bar_today['euro'])}} Euro</span>
                @elseif($total_bar_today['euro']=='0')
                <span class="info-box-number">{{$total_bar_today['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.members')}}</span>
                <span class="info-box-number">{{$members_online_now}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>

        <h5 class="mb-2">{{__('cms.to_now')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.karte')}}</span>
                @if($total_karte['euro']!='0' && $total_karte['cent']!='0')
                <span class="info-box-number">{{number_format($total_karte['euro'])}} Euro {{$total_karte['cent']}} Cent</span>

                @elseif($total_karte['cent']=='0')

                <span class="info-box-number">{{number_format($total_karte['euro'])}} Euro</span>
                @elseif($total_karte['euro']=='0')
                <span class="info-box-number">{{$total_karte['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.kasse')}}</span>
                @if($total_kasse['euro']!='0' && $total_kasse['cent']!='0')
                <span class="info-box-number">{{number_format($total_kasse['euro'])}} Euro {{$total_kasse['cent']}} Cent</span>

                @elseif($total_bar['cent']=='0')

                <span class="info-box-number">{{number_format($total_kasse['euro'])}} Euro</span>
                @elseif($total_kasse['euro']=='0')
                <span class="info-box-number">{{$total_kasse['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.bar')}}</span>
                @if($total_bar['euro']!='0' && $total_bar['cent']!='0')
                <span class="info-box-number">{{number_format($total_bar['euro'])}} Euro {{$total_bar['cent']}} Cent</span>

                @elseif($total_bar['cent']=='0')

                <span class="info-box-number">{{number_format($total_bar['euro'])}} Euro</span>
                @elseif($total_bar['euro']=='0')
                <span class="info-box-number">{{$total_bar['cent']}} Cent</span>
                
                @else
                <span class="info-box-number">0</span>
                @endif
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="ion ion-stats-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.members')}}</span>
                <span class="info-box-number">{{$memberscount}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>


        <!-- Small boxes (Stat box) -->
        <div class="row">


         
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$reportcount}}</h3>

                <p>{{__('cms.report')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$branchcount}}</h3>

                <p>{{__('cms.branches')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('branches.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>


          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$members_online_now}}</h3>

                <p>{{__('cms.online_member')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{route('online.member')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->


          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$memberscount}}</h3>

                <p>{{__('cms.all_members')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{route('members.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          

            <!-- ./col -->
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3></h3>

                <p>{{__('cms.data')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{route('file.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <h3 class="mt-4 mb-4">{{__('cms.branch')}}</h3>

<div class="row">
 
@foreach ($branches as $branch)
<div class="col-md-4">
    <!-- Widget: user widget style 1 -->
    <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-info">
        <h3 class="widget-user-username">{{$branch->title}}</h3>
        <h5 class="widget-user-desc">{{$branch->postal_code}}</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle elevation-2" src="{{asset('template/images/Default-welcomer.png')}}" alt="User Avatar">
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-sm-4 border-right">
            <div class="description-block">
            <a href="{{route('report.list.branch',['branch'=>$branch->id])}}">
              <span class="description-text" style="font-size:15px">{{__('cms.time')}}</span>
</a>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4 border-right">
            <div class="description-block">
            
            <a href="{{route('shop.list.branch',['branch'=>$branch->id])}}"> <span class="description-text" style="font-size:15px">{{__('cms.shopping_list')}}</span>
</a>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4">
            <div class="description-block">
          <a href="{{route('accounting.list.branch',['branch'=>$branch->id])}}">              <span class="description-text" style="font-size:15px">{{__('cms.accounting')}}</span>
</a>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
@endforeach
 
  <!-- /.col -->
 
</div>
<!-- /.row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    


@endsection