@include('core::layout.modules.index',[

    'title'=>__('core::branches.index'),
    'items'=>$items,
    'parent'=>'core',
    'model'=>'branche',
    'class_model'=> 'branche',
    'directory'=>'branches',
    'collect'=>__('core::branches.collect'),
    'singular'=>__('core::branches.singular'),
    'create_route'=>['name'=>'branches.create'],
    'edit_route'=>['name'=>'branches.edit','name_param'=>'branch'],
    'search_route'=>true,
    'route_report_branch'=>true,
     'pagination'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',

     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
