<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $fillable = ['title','description','status'];
}
