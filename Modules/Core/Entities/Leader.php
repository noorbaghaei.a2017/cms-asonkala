<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Leader extends Model
{
    use TimeAttribute,Sluggable;
    protected $fillable = ['parent','level','icon','model','slug','excerpt','token','order','user','title','symbol'];

    protected $table='leaders';

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
