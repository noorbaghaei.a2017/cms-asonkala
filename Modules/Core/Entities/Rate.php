<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table="rates";
    protected $fillable = ['client','rate'];

    public function rateable()
    {
        return $this->morphTo();
    }
}
