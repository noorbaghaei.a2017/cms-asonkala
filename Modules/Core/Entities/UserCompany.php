<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;

class UserCompany extends Model
{
    protected $table="user_companies";

    protected $fillable = ['name','phone','national_id','website','postal_code','address','mobile','count_member','country', 'state', 'city', 'area'];

    public function companyable(){
        return $this->morphTo();
    }

    public function client_info(){
        return $this->belongsTo(Client::class,'client','id');
    }

}
