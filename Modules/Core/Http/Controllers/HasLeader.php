<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Article\Entities\Article;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Leader;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Core\Http\Requests\LeaderRequest;

trait HasLeader
{
    public  function leaders(){

        try {

            $items=Leader::latest()->where('model',$this->class)->get();
            return view($this->route_leaders_index,compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function leaderCreate(){
        try {
            $parent_leaders=Leader::latest()->whereParent(0)->where('model',$this->class)->get();
            return view($this->route_leaders_create,compact('parent_leaders'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function leaderStore(LeaderRequest $request){
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Leader::whereToken($request->input('parent'))->first();
            }

            $saved=Leader::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'excerpt'=>$request->input('excerpt'),
                'model'=>$this->class,
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('core::leaders.error'));
            }else{
                return redirect(route($this->route_leaders))->with('message',__('core::leaders.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function LeaderEdit(Request $request,$token){
        try {

            $item=Leader::whereToken($token)->first();
            $parent_leaders=Leader::latest()->whereParent(0)->where('model',$this->class)->where('token','!=',$token)->get();

            return view($this->route_leaders_edit,compact('item','parent_leaders'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function leaderUpdate(Request $request,$token){
        try {


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Leader::whereToken($request->input('parent'))->first();
            }

            $category=Leader::whereToken($token)->first();
            $updated=$category->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_leaders))->with('message',__('core::leaders.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

}
