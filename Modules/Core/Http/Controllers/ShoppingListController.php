<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Entities\Branch;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Artesaos\SEOTools\Facades\SEOMeta;
use Modules\Member\Entities\MemberReport;
use Modules\Member\Entities\Member;

class ShoppingListController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new MemberReport();

        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->get();
            return view('core::layout.shopping_list.index',compact('items'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }

    public function shop(Request $request,$branch)
    {
        try {
            $branch_item=Branch::find($branch);
            $items=$this->entity->latest()->where('status_shop',0)->where('branch',$branch_item->id)->get();
            return view('core::layout.shopping_list.index',compact('items','branch_item'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }
    public function accounting(Request $request,$branch)
    {
        try {
            $branch_item=Branch::find($branch);
           
            
            $items=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->get();
            $karte=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('karte')->pluck('karte')->toArray();
            $kasse=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('kasse')->pluck('kasse')->toArray();
            $bar=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('bar')->pluck('bar')->toArray();

            
           $calc_karte=calcEuro($karte);
           $calc_kasse=calcEuro($kasse);
           $calc_bar=calcEuro($bar);

            
            
            return view('core::layout.shopping_list.accounting',compact('items','branch_item','calc_karte','calc_kasse','calc_bar'));
       
        }catch (\Exception $exception){
        
            
            return abort('500');
        }
    }
    public function yesterdayAccounting(Request $request,$branch)
    {
        try {
           
            $branch_item=Branch::find($branch);
           
            
            $items=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->subMonth())->get();
            $karte=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('karte')->pluck('karte')->toArray();
            $kasse=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('kasse')->pluck('kasse')->toArray();
            $bar=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->subMonth())->whereNotNull('bar')->pluck('bar')->toArray();

            
           $calc_karte=calcEuro($karte);
           $calc_kasse=calcEuro($kasse);
           $calc_bar=calcEuro($bar);

            
            
            return view('core::layout.shopping_list.yesterday-accounting',compact('items','branch_item','calc_karte','calc_kasse','calc_bar'));
       
        }catch (\Exception $exception){
        
            
            return abort('500');
        }
    }
    public function testAccounting(Request $request,$branch)
    {
        try {

            $min=0;
            $hour=0;
            $list=MemberReport::whereMember(9)->whereMonth('created_at',  Carbon::now()->subMonth())->get();
            foreach($list as $value){
                $min=$min+(int)$value->today_minu;
                $hour=$hour+(int)$value->today_hour;
                $array_min[]=(int)$value->today_minu;
                $array_hour[]=(int)$value->today_hour;
               
            }

            $t=fullDivNumber($min,60);
            return $hour+$t['mod2'];
            return dd($min,$hour,$array_min,$array_hour,("hour : ".$hour."minu : ".$min));



            $branch_item=Branch::find($branch);
           
            
            $items=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->get();
            
           
            $karte=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('karte')->pluck('karte')->toArray();
            $kasse=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('kasse')->pluck('kasse')->toArray();
            $bar=$this->entity->latest()->where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->whereNotNull('bar')->pluck('bar')->toArray();

            
           $calc_karte=calcEuro($karte);
           $calc_kasse=calcEuro($kasse);
           $calc_bar=calcEuro($bar);

            
            
            return view('core::layout.shopping_list.accounting',compact('items','branch_item','calc_karte','calc_kasse','calc_bar'));
       
        }catch (\Exception $exception){
        
           
            return abort('500');
        }
    }
    public function createAccounting(Request $request,$branch)
    {
        try {
            $branch_item=Branch::find($branch);
            return view('core::layout.shopping_list.create',compact('branch_item'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }

    public function shoppingDeactive(Request $request,$report,$branch){
        try {
            DB::beginTransaction();

            $record=MemberReport::findOrFail($report);
           

            $record->update([
                'status_shop'=>1,
                'price_mode'=>$request->price_mode
            ]);

            $branch_item=Branch::find($branch);

            $items=$this->entity->latest()->where('status_shop',0)->where('branch',$branch_item->id)->get();

           
            DB::commit();
            return redirect()->back()->with('message',__('product::products.store'));

        }catch (\Exception $exception){
            DB::rollBack();
            
            return redirect()->back()->with('error',__('product::products.error'));

          
        }
    }

    public function storeAccounting(Request $request,$branch)
    {
        try {

            DB::beginTransaction();
            $branch_item=Branch::find($branch);

            MemberReport::create([
                'start'=>Carbon::now()->addHours(2),
                'end'=>Carbon::now()->addHours(2),
                'end_ip'=> $_SERVER['REMOTE_ADDR'],
                'text'=>'',
                'branch'=>$branch_item->id,
                'end_longitude'=>null,
                'end_latitude'=>null,
                'start_browser'=>$_SERVER['HTTP_USER_AGENT'],
                'end_browser'=>$_SERVER['HTTP_USER_AGENT'],
                'income'=>'',
                'hulle'=>$request->hulle,
                'tasche'=>$request->tasche,
                'glas'=>$request->glas,
                'zubehhor'=>$request->zubehhor,
                'kasse'=>$request->kasse,
                'karte'=>$request->karte,
                'bar'=>$request->bar,
            ]);
       
            if($request->has('image')){
                $member_report->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            
            $items=$this->entity->latest()->where('branch',$branch_item->id)->get();

     
            DB::commit();
            return redirect()->route('shop.list.branch',['branch'=>$branch_item->id])->with('message',__('product::products.store'));
        }catch (\Exception $exception){
          
            DB::rollBack();
            return redirect()->back()->with('error',__('product::products.error'));

        }
    }

    public function tasche()
    {
        try {
                      
            $members=Member::whereBranch(4)->pluck('id')->toArray();
            $items=$this->entity->latest()->whereIn('member',$members)->get();
            return view('core::layout.shopping_list.tasche',compact('items'));
        }catch (\Exception $exception){
          
           
            return abort('500');
        }
    }
    public function glas()
    {
        try {
           
            $members=Member::whereBranch(1)->pluck('id')->toArray();
            $items=$this->entity->latest()->whereIn('member',$members)->get();
            return view('core::layout.shopping_list.glas',compact('items'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }
    public function file()
    {
        try {
           
           
            $items=$this->entity->latest()->get();
            return view('core::layout.shopping_list.file',compact('items'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }
    public function zubehhor()
    {
        try {
            
            $members=Member::whereBranch(3)->pluck('id')->toArray();
            $items=$this->entity->latest()->whereIn('member',$members)->get();
            return view('core::layout.shopping_list.zubehhor',compact('items'));
        }catch (\Exception $exception){
          
            
            return abort('500');
        }
    }
    public function hulle()
    {
        try {
           
            $members=Member::whereBranch(2)->pluck('id')->toArray();
            $items=$this->entity->latest()->whereIn('member',$members)->get();
            return view('core::layout.shopping_list.hulle',compact('items'));
        }catch (\Exception $exception){
          
           
            return abort('500');
        }
    }
       /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
           
            !isset($request->text) &&
            !isset($request->branch)

            ){
                $items=$this->entity->latest()->whereNotNull('text')->orWhere('text','<>','')->paginate(config('cms.paginate'));
                return view('core::layout.shopping_list.index',compact('items'));
            }
            $member=Member::where('branch',$request->branch)->pluck('id')->toArray();
             $items=$this->entity
             ->where('text', 'like', '%' . trim($request->text) . '%')
             ->whereIn('member', $member)
             
             
                ->paginate(config('cms.paginate'));

            return view('core::layout.shopping_list.index',compact('items','request'));
        }catch (\Exception $exception){
          return dd($exception);
            return abort('500');
        }
    }

   
}
