<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

trait HasQuestion
{


    public  function question($token){
        try {

            $item=$this->entity->whereToken($token)->first();
            $items=$item->questions;

            return view($this->route_questions_index,compact('items','token'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function questionCreate($token){
        try {

            return view($this->route_questions_create,compact('token'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function questionStore(QuestionRequest $request,$token){
        try {

            $this->entity=$this->entity->whereToken($token)->first();
            $saved=$this->entity->questions()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'order'=>orderInfo($request->input('order')),
                'answer'=>$request->input('answer'),
                'token'=>tokenGenerate()
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_questions))->with('message',__($this->notification_store));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function questionEdit(Request $request,$article,$question){
        try {
            $token=$article;
            $item=Question::whereToken($question)->first();

            return view($this->route_questions_edit,compact('token','item'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function questionUpdate(QuestionRequest $request,$question){
        try {

            $item=Question::whereToken($question)->first();
            $updated=$item->update([
                'title'=>$request->input('title'),
                'order'=>orderInfo($request->input('order')),
                'answer'=>$request->input('answer')
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_questions))->with('message',__($this->notification_update));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function questionDestroy(Request $request,$token){
        try {
            $question=Question::whereToken($token)->first();
            $deleted=$question->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_questions))->with('message',__($this->notification_delete));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

}
