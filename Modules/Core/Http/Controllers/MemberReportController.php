<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Entities\Branch;
use Modules\Member\Entities\Member;
use Illuminate\Routing\Controller;
use Modules\Member\Entities\MemberReport;
use App\Exports\MemberReportExport;
use \Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class MemberReportController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new MemberReport();

        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('core::reports.index',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }


    }
    public function report($branch)
    {
        try {
            $branch_item=Branch::find($branch);
            $items=MemberReport::where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->month)->orderBy('id','desc')->get();
            return view('core::reports.index',compact('items','branch_item'));
        }catch (\Exception $exception){
          
            return abort('500');
        }


    }
    public function yesterdayReport($branch)
    {
        try {
            $branch_item=Branch::find($branch);
            $items=MemberReport::where('branch',$branch_item->id)->whereMonth('created_at', Carbon::now()->subMonth())->orderBy('id','desc')->get();
            return view('core::reports.yesterday-index',compact('items','branch_item'));
        }catch (\Exception $exception){
          
            return abort('500');
        }


    }

     /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
           
            !isset($request->email)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('core::reports.index',compact('items'));
            }
             $items=$this->entity
             ->where('email', 'like', '%' . trim($request->email) . '%')
             
             
                ->paginate(config('cms.paginate'));
            return view('core::reports.index',compact('items','request'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    public function export() 
    {
        return Excel::download(new MemberReportExport, Carbon::now()->addHour(2).'.member_report.xlsx');
    }

  
}
