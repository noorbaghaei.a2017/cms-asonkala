<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Option;
use Modules\Core\Http\Requests\OptionRequest;
use Modules\Product\Transformers\ProductOption\ProductOptionCollection;


trait HasOption
{
    public  function options($token){

        try {
           $items=$this->repository->getOption($token);

            $result = new ProductOptionCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function optionCreate($token){
        try {

            $data=$this->entity->whereToken($token)->firstOrFail();
            return view($this->route_options_create,compact('data'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function optionStore(OptionRequest $request,$data){
        try {
            DB::beginTransaction();
            $data=$this->entity->whereToken($data)->firstOrFail();

            $saved=$data->options()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'level'=>isset($level) ? $level : 0,
                'icon'=>$request->input('icon'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);

            if($request->has('image')){
                $saved->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('core::options.error'));
            }else{
                DB::commit();
                return redirect(route($this->route_options,['token'=>$data->token]))->with('message',__('core::options.store'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function OptionEdit(Request $request,$token){
        try {

            $item=Option::whereToken($token)->first();
            $parent_options=Option::latest()->where('optionable_id',$this->class)->where('token','!=',$token)->get();

            return view($this->route_options_edit,compact('item','parent_options'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function optionUpdate(OptionRequest $request,$option){
        try {
            DB::beginTransaction();
            $item=Option::whereToken($option)->firstOrFail();
            $data=$this->entity->findOrFail($item->optionable_id);

            $updated=$item->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'level'=>isset($level) ? $level : 0,
                'status'=>$request->input('status'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
            ]);

            if($request->has('image')){
                destroyMedia($item,config('cms.collection-image'));
                $item->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                DB::commit();
                return redirect(route($this->route_options,['token'=>$data->token]))->with('message',__('core::options.store'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

}
