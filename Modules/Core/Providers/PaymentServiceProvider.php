<?php

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\Passargad;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('melat',function (){
            return new Melat();
        });
        $this->app->bind('passargad',function (){
            return new Passargad();
        });
        $this->app->bind('zarinpal',function (){
            return new ZarinPal();
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
