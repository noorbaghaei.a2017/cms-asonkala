<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sms\Http\Controllers\Gateway\FarazSms;
use Modules\Sms\Http\Controllers\Gateway\Idepardazan;
use Modules\Sms\Http\Controllers\Gateway\Kavenegar;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('farazsms',function (){
            return new Farazsms();
        });
        $this->app->bind('kavenegar',function (){
            return new Kavenegar();
        });
        $this->app->bind('idepardazan',function (){
            return new Idepardazan();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
