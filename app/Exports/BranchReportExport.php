<?php

namespace App\Exports;

use Modules\Member\Entities\MemberReport;
use Modules\Member\Entities\Member;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class BranchReportExport implements FromView,WithEvents
{
    private $branch;
    public function __construct($branch)
    {
       
        $this->branch = $branch; 
    }
    public function view(): View
    {

        $array_member=Member::whereBranch($this->branch)->pluck('id')->toArray();
        return view('template.exports.report_branche', [
            'reports' => MemberReport::whereIn('member',$array_member)->get()
        ]);
    }
        public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(25);  
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(60);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(20);
               
            }
        ];
    }
}
