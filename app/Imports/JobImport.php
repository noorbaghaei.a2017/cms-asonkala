<?php

namespace App\Imports;

use Modules\Core\Entities\UserServices;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class JobImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      
        
        return new UserServices([
           
            'user'=>intval($row['user']),
            'service'=>intval($row['service']),
            'title'=>$row['title'],
            'excerpt'=>$row['excerpt'],
            'longitude'=>'',
            'latitude'=>'',
            'country'=>'german',
            'google_map'=>'',
            'status'=>0,
            'city'=>$row['city'],
            'postal_code'=>$row['postal_code'],
            'address'=>$row['address'],
            'email'=>$row['email'],
            'phone'=>$row['phone'],
            'token'=>tokenGenerate()

        ]);
       

       
    }
}
