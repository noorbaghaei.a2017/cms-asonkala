@extends('template.app')

@section('content')

    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="{{asset('template/images/bg/02.jpg')}}">
        <div class="container">
            <div class="row">
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--about start-->

        <section>
            <div class="container">
                <div class="row align-items-center page-about-us">
                    <div class="col-lg-6 col-md-12">
                        <form class="demo-form" action="#" method="GET" style="margin: 100px 10px" id="supplier-form">

                            <h5>
                                {{__('cms.supplier-form')}}
                            </h5>
                            <div class="form-section">
                                <label for="kind">{{__('cms.kind')}} *:</label>
                                <p>

                                    {{__('cms.manufacturer')}}: <input type="radio" name="kind"  value="manufacturer" required="">

                                    {{__('cms.vendor-company')}}: <input type="radio" name="kind"  value="vendor-company" >

                                    {{__('cms.shop')}}: <input type="radio" name="kind"  value="shop" >
                                </p>

                                <label for="company-name">{{__('cms.company-name')}} *:</label>
                                <input type="text" class="form-control" name="company-name" required="" autocomplete="off">



                                <label for="company-type">{{__('cms.company-type')}} *:</label>
                                <p>


                                    {{__('cms.private-equity')}}: <input type="radio" name="company-type"  value="private-equity" required="">


                                    {{__('cms.with-limited-responsibility')}}: <input type="radio" name="company-type"  value="with-limited-responsibility" >


                                    {{__('cms.other-cases')}}: <input type="radio" name="company-type"  value="other-cases" >
                                </p>

                                <label for="field-of-activity">{{__('cms.field-of-activity')}} *:</label>
                                <p>


                                    {{__('cms.production')}}: <input type="radio" name="field-of-activity"  value="production" required="">


                                    {{__('cms.sales')}}: <input type="radio" name="field-of-activity"  value="sales" >


                                    {{__('cms.services')}}: <input type="radio" name="field-of-activity"  value="services" >
                                </p>
                                <label for="type-of-ownership">{{__('cms.type-of-ownership')}} *:</label>
                                <p>


                                    {{__('cms.private')}}: <input type="radio" name="type-of-ownership"  value="private" required="">


                                    {{__('cms.governmental')}}: <input type="radio" name="type-of-ownership"  value="governmental" >


                                    {{__('cms.other-cases')}}: <input type="radio" name="type-of-ownership"  value="other-cases" >
                                </p>

                                <label for="product-name">{{__('cms.product-name')}}:</label>
                                <input type="text" class="form-control" name="product-name" required="" autocomplete="off">

                                <label for="registration-number">{{__('cms.registration-number')}}:</label>
                                <input type="text" class="form-control" name="registration-number" required="" autocomplete="off">

                                <label for="operating-license-number">{{__('cms.operating-license-number')}}:</label>
                                <input type="text" class="form-control" name="operating-license-number" required="" autocomplete="off">

                                <label for="company-name">{{__('cms.economic-code')}}:</label>
                                <input type="text" class="form-control" name="economic-code" required="" autocomplete="off">

                                <label for="economic-code">{{__('cms.national-id')}}:</label>
                                <input type="text" class="form-control" name="economic-code" required="" autocomplete="off">

                                <label for="business-license-number">{{__('cms.business-license-number')}}:</label>
                                <input type="text" class="form-control" name="business-license-number" required="" autocomplete="off">


                                <label for="date-of-establishment">{{__('cms.date-of-establishment')}}:</label>
                                <input type="text" class="form-control" name="date-of-establishment" required="" autocomplete="off">

                            </div>

                            <div class="form-section">

                                <label for="is-there-codified-training-system-for-the-unit-staff">{{__('cms.is-there-codified-training-system-for-the-unit-staff')}} *:</label>
                                <input type="text" class="form-control" name="is-there-codified-training-system-for-the-unit-staff" required="" autocomplete="off">


                                <label for="number-of-third-party-inspector-certification-during-the-past-year">{{__('cms.number-of-third-party-inspector-certification-during-the-past-year')}} *:</label>
                                <input type="text" class="form-control" name="number-of-third-party-inspector-certification-during-the-past-year" required="" autocomplete="off">

                                <label for="do-you-have-history-of-exporting-product-or-service-during-the-last-5-years">{{__('cms.do-you-have-history-of-exporting-product-or-service-during-the-last-5-years')}} *:</label>
                                <input type="text" class="form-control" name="do-you-have-history-of-exporting-product-or-service-during-the-last-5-years" required="" autocomplete="off">


                                <label for="export-amount-during-the-last-5-years">{{__('cms.export-amount-during-the-last-5-years')}} *:</label>
                                <input type="text" class="form-control" name="export-amount-during-the-last-5-years" required="" autocomplete="off">

                                <label for="do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company">{{__('cms.do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company')}} :</label>
                                <input type="text" class="form-control" name="do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company" required="" autocomplete="off">

                                <label for="do-you-have-distribution-network-if-yes-please-name-the-distribution-locations">{{__('cms.do-you-have-distribution-network-if-yes-please-name-the-distribution-locations')}} *:</label>
                                <input type="text" class="form-control" name="do-you-have-distribution-network-if-yes-please-name-the-distribution-locations" autocomplete="off">

                                <label for="do-you-have-equipment-for-packing-goods">{{__('cms.do-you-have-equipment-for-packing-goods')}} *:</label>
                                <input type="text" class="form-control" name="do-you-have-equipment-for-packing-goods" autocomplete="off">



                                <label for="are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it">{{__('cms.are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it')}} *:</label>
                                <input type="text" class="form-control" name="are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it" required="" autocomplete="off">


                                <label for="how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much">{{__('cms.how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much')}} *:</label>
                                <input type="text" class="form-control" name="how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much" required="" autocomplete="off">



                            </div>

                            <div class="form-navigation">
                                <button type="button" class="previous btn btn-info pull-left">{{__('cms.previous')}}</button>
                                <button type="button" class="next btn btn-info pull-right">{{__('cms.next')}}</button>
                                <input type="submit" class="btn btn-info pull-right" value="{{__('cms.send')}}">
                                <span class="clearfix"></span>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!--about end-->



    </div>

    <!--body content end-->
@endsection

@section('heads')

    {{--<link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">--}}
    <link href="{{asset('template/css/parsley.css')}}" rel="stylesheet" type="text/css">

    <style>
        .form-section {
            padding-left: 15px;
            border-left: 2px solid #FF851B;
            display: none;
        }
        .form-section.current {
            display: inherit;
        }
        .btn-info, .btn-default {
            margin-top: 10px;
        }
        .btn-info{color:#fff;background-color:#5bc0de;border-color:#5bc0de}
        .btn-info:hover{color:#fff;background-color:#31b0d5;border-color:#2aabd2}
        .btn-info.focus,.btn-info:focus{color:#fff;background-color:#31b0d5;border-color:#2aabd2}
    </style>
@endsection

@section('scripts')

    <script src="{{asset('template/js/parsley.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            var $sections = $('.form-section');

            function navigateTo(index) {
                // Mark the current section with the class 'current'
                $sections
                    .removeClass('current')
                    .eq(index)
                    .addClass('current');
                // Show only the navigation buttons that make sense for the current section:
                $('.form-navigation .previous').toggle(index > 0);
                var atTheEnd = index >= $sections.length - 1;
                $('.form-navigation .next').toggle(!atTheEnd);
                $('.form-navigation [type=submit]').toggle(atTheEnd);
            }

            function curIndex() {
                // Return the current index by looking at which section has the class 'current'
                return $sections.index($sections.filter('.current'));
            }

            // Previous button is easy, just go back
            $('.form-navigation .previous').click(function() {
                navigateTo(curIndex() - 1);
            });

            // Next button goes forward iff current block validates
            $('.form-navigation .next').click(function() {
                $('.demo-form').parsley().whenValidate({
                    group: 'block-' + curIndex()
                }).done(function() {
                    navigateTo(curIndex() + 1);
                });
            });

            // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
            $sections.each(function(index, section) {
                $(section).find(':input').attr('data-parsley-group', 'block-' + index);
            });
            navigateTo(0); // Start at the beginning
        });
    </script>

@endsection








