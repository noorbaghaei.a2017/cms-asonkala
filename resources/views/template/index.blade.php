﻿<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<title>
ganzjob
</title>
</head>


<body>
<link href="{{asset('template/css/style.css')}}" rel="stylesheet">

<link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<script src="{{asset('template/js/jquery.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-lg-6">
           
            <div class="account-wall">

            <h1 id="time" style="text-align:center;">{{getDateTime()}} </h1>
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">

                <form class="form-signin" action="{{ route('member.login.submit') }}" method="POST" >
                @csrf
				@error('email')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
				<input type="text" name="identify" class="form-control" placeholder="E-mail" required autofocus autocomplete="off">
				@error('password')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
			    <input type="password" name="password" class="form-control" placeholder="Passwort" required autocomplete="off">
				<input hidden id="longitude" name="longitude" value="">
<input hidden id="latitude" name="latitude" value="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">
				Anmelden</button>
              
                </form>

				
            </div>
         
        </div>
    </div>
</div>


<script>
  if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                position => {
                  
                   
                      $("#longitude").attr('value',position.coords.longitude);
                      $("#latitude").attr('value',position.coords.latitude);
                    
                },
                error => {
                   
                    $("#longitude").attr('value','');
                      $("#latitude").attr('value','');
                     
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000});
        }

</script>



</body>

</html>