@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/35.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                        @if(!isset($queries))

                           
<div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
<a href="{{route('front.website')}}">{{__('cms.home')}}</a>
@if(showServiceCategory($category)->parent!='0')
<a href="{{route('search.page.bussiness',['category'=>showChildCategoryJob($category)->id])}}">{{convert_lang(showChildCategoryJob($category),LaravelLocalization::getCurrentLocale(),'title')}}</a> 
@endif
<span> {{convert_lang(showServiceCategory($category),LaravelLocalization::getCurrentLocale(),'title')}}</span> 

</div>
@else

@endif
                            <!-- list-main-wrap-header-->
                            <div class="list-main-wrap-header fl-wrap   block_box no-vis-shadow no-bg-header fixed-listing-header">
                                <!-- list-main-wrap-title-->
                               
                                <!-- list-main-wrap-title end-->
                                <!-- list-main-wrap-opt-->
                                <div class="list-main-wrap-opt">
                                    
                                    <!-- price-opt-->
                                    <div class="grid-opt">
                                        <ul class="no-list-style">
                                            <li class="grid-opt_act"><span class="two-col-grid act-grid-opt tolt" data-microtip-position="bottom" data-tooltip="نمایش شبکه ای"><i class="fal fa-th"></i></span></li>
                                            <li class="grid-opt_act"><span class="one-col-grid tolt" data-microtip-position="bottom" data-tooltip="نمایش لیستی"><i class="fal fa-list"></i></span></li>
                                        </ul>
                                    </div>
                                    <!-- price-opt end-->
                                </div>
                                <!-- list-main-wrap-opt end-->                    
                                <a class="custom-scroll-link back-to-filters clbtg" href="#lisfw"><i class="fal fa-search"></i></a>
                            </div>
                            <!-- list-main-wrap-header end-->                      
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  {{__('cms.filter')}}</div>
                            <div class="fl-wrap">
                                <!-- listsearch-input-wrap-->
                                <div class="listsearch-input-wrap lws_mobile fl-wrap tabs-act inline-lsiw" id="lisfw">
                                    <div class="listsearch-input-wrap_contrl fl-wrap">
                                        <ul class="tabs-menu fl-wrap no-list-style">
                                            <li class="current"><a href="#filters-search"> <i class="fal fa-sliders-h"></i> {{__('cms.filter')}} </a></li>
                                            <li><a href="#category-search"> <i class="fal fa-image"></i>  {{__('cms.category')}} </a></li>
                                        </ul>
                                    </div>
                                  
                                    <!--tabs -->                       
                                    <div class="tabs-container fl-wrap">
                                    <form action="{{route('search.page.bussiness')}}" method="GET">

                                        <!--tab -->
                                        <div class="tab">
                                            <div id="filters-search" class="tab-content  first-tab ">
                                                <div class="fl-wrap">
                                                    <div class="row">


                                                        <!-- listsearch-input-item-->
                                                        <div class="col-md-4">
                                                            <div class="listsearch-input-item">
                                                                <span class="iconn-dec"><i class="far fa-bookmark"></i></span>
                                                                <input type="text" name="title" placeholder=" {{__('cms.name')}} "  value=""/>
                                                            </div>
                                                        </div>
                                                        <!-- listsearch-input-item end-->


                                                        <!-- listsearch-input-item-->
                                                        <div class="col-md-4">
                                                            <div class="listsearch-input-item">
                                                                <span class="iconn-dec"><i class="far fa-bookmark"></i></span>
                                                                <input id="postal_code" onkeyup="loadCity(event)" type="text" name="postal_code" placeholder=" {{__('cms.postal_code')}} "  value=""/>
                                                            </div>
                                                        </div>
                                                        <!-- listsearch-input-item end-->

                                                         <!-- listsearch-input-item-->
                                                         <div class="col-md-4">
                                                            <div class="listsearch-input-item">
                                                            <select id="city" name="city" data-placeholder="City/Location" class="chosen-select no-search-select" >
                                                                    
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- listsearch-input-item end-->

                                                       
                                                        <!-- listsearch-input-item-->
                                                        <div class="col-md-4">
                                                            <div class="listsearch-input-item">
                                                                <select name="category" data-placeholder="City/Location" class="chosen-select no-search-select" >
                                                                    <option selected>{{__('cms.all')}} </option>
                                                                     @foreach ($all_category_bussiness as $category)
                                                                             <option value="{{$category->id}}"> {!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                                                         @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- listsearch-input-item end-->  
                                                       
                                                                                                       
                                                        <!-- listsearch-input-item-->
                                                        <div class="col-md-2">
                                                            <div class="listsearch-input-item">
                                                                <button type="submit" class="header-search-button color-bg" ><i class="far fa-search"></i><span>{{__('cms.search')}}</span></button>
                                                            </div>
                                                        </div>
                                                        <!-- listsearch-input-item end-->                                         
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <!--tab end-->
                                        </form>
                                        <!--tab --> 
        
                                        <div class="tab">
                                            <div id="category-search" class="tab-content">
                                                <!-- category-carousel-wrap -->  
                                                <div class="category-carousel-wrap fl-wrap">
                                                    <div class="category-carousel fl-wrap full-height">
                                                        <div class="swiper-container">
                                                            <div class="swiper-wrapper">

                                                            @foreach ($more_view_category_bussiness as $category)
                                                                
                                                             <!-- category-carousel-item -->  
                                                             <div class="swiper-slide">
                                                                    <a href="{{route('search.page.bussiness',['category'=>$category->id])}}" class="category-carousel-item fl-wrap full-height" >
                                                                        <img src="{{asset('template/images/all/12.jpg')}}" alt="">
                                                                        <div class="category-carousel-item-icon red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                                        <div class="category-carousel-item-container">
                                                                            <div class="category-carousel-item-title">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</div>
                                                                            <div class="category-carousel-item-counter">{{$category->user_services()->count()}} {{__('cms.job')}}</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- category-carousel-item end -->  
                                                              
                                                            @endforeach
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- category-carousel-wrap end-->  
                                                </div>
                                                <div class="catcar-scrollbar fl-wrap">
                                                    <div class="hs_init"></div>
                                                    <div class="cc-contorl">
                                                        <div class="cc-contrl-item cc-next"><i class="fal fa-angle-left"></i></div>
                                                        <div class="cc-contrl-item cc-prev"><i class="fal fa-angle-right"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--tab end-->
                                    </div>
                                    <!--tabs end-->
                                </div>
                                <!-- listsearch-input-wrap end-->                                
                                <!-- listing-item-container -->
                                <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic three-columns-grid">
                                   
                                     @if(isset($queries))
                                     @foreach ($queries as $query )
                               
                               <div class="listing-item">
                                   <article class="geodir-category-listing fl-wrap">
                                       <div class="geodir-category-img">
                                           <a href="{{route('bussiness.single',['bussiness'=>$query->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                           @if(!$query->Hasmedia('images'))
                                                                   <img  src="{{asset('template/images/no-image.jpeg')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$query->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif
                                           </a>
                                         
                                           <div class="geodir-category-opt">
                                               <div class="listing-rating-count-wrap">
                                                 
                                                   <br>
                                                   <div class="reviews-count">{{count($query->comments)}} {{__('cms.comment')}}</div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="geodir-category-content fl-wrap title-sin_item">
                                           <div class="geodir-category-content-title fl-wrap">
                                               <div class="geodir-category-content-title-item">
                                                   <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$query->slug])}}">{{convert_lang($query,LaravelLocalization::getCurrentLocale(),'title')}} </a></h3>
                                                   <div class="geodir-category-location fl-wrap"><a href="#" ><i class="fas fa-map-marker-alt"></i> {{$query->city}}</a></div>
                                               </div>
                                           </div>
                                           <div class="geodir-category-text fl-wrap">
                                               <p class="small-text"> {{convert_lang($query,LaravelLocalization::getCurrentLocale(),'title')}}</p>
                                               <div class="facilities-list fl-wrap">
                                                   <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                   <ul class="no-list-style">
                                                   @foreach ($query->attributes as $attr)
                                                        <li class="tolt"  data-microtip-position="top" data-tooltip=" {{getAttribute($attr->attribute)->title}}"><i class="{{getAttribute($attr->attribute)->icon}}"></i></li>
   
                                                        @endforeach
                                                    </ul>
                                               </div>
                                           </div>
                                           <div class="geodir-category-footer fl-wrap">
                                               <a class="listing-item-category-wrap">
                                                   <div class="listing-item-category category-box"><i class="fal fa-cheeseburger"></i></div>
                                                   <span> {{convert_lang(showServiceCategory($query->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                                               </a>
                                               <div class="geodir-opt-list">
                                               <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$query->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($query->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($query->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                                               </div>
                                              
                                               <div class="geodir-category_contacts">
                                                   <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                   <ul class="no-list-style">
                                                       <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$query->phone}}</a></li>
                                                       <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$query->email}}</a></li>
                                                   </ul>
                                               </div>
                                           </div>
                                       </div>
                                   </article>
                               </div>
                            
                               @endforeach   

                                     @else

                                     @foreach ($all_bussiness as $bussiness )

<!-- listing-item  -->
<div class="listing-item">
    <article class="geodir-category-listing fl-wrap">
        <div class="geodir-category-img">
            <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
            @if(!$bussiness->Hasmedia('images'))
                                    <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                @else
                                <img  src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                @endif
            </a>
           
            <div class="geodir-category-opt">
                <div class="listing-rating-count-wrap">
                  
                  <br>
                    <div class="reviews-count">{{count($bussiness->comments)}} {{__('cms.comment')}}</div>
                </div>
            </div>
        </div>
        <div class="geodir-category-content fl-wrap title-sin_item">
            <div class="geodir-category-content-title fl-wrap">
                <div class="geodir-category-content-title-item">
                    <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}">{{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'title')}} </a></h3>
                    <div class="geodir-category-location fl-wrap"><a href="#" ><i class="fas fa-map-marker-alt"></i> {{$bussiness->city}}</a></div>
                </div>
            </div>
                                          <div class="geodir-category-text fl-wrap">
                                               <p class="small-text"> {{$bussiness->title}}</p>
                                               <div class="facilities-list fl-wrap">
                                                   <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                   <ul class="no-list-style">
                                                   @foreach ($bussiness->attributes as $attr)

                                                   <li class="tolt"  data-microtip-position="top" data-tooltip=" {{getAttribute($attr->attribute)->title}}"><i class="{{getAttribute($attr->attribute)->icon}}"></i></li>

                                                        @endforeach
                                                    </ul>
                                               </div>
                                           </div>
            <div class="geodir-category-footer fl-wrap">
                <a class="listing-item-category-wrap">
                    <div class="listing-item-category category-box"><i class="fal fa-cheeseburger"></i></div>
                    <span> {{convert_lang(showServiceCategory($bussiness->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                </a>
                <div class="geodir-opt-list">
                <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$bussiness->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($bussiness->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($bussiness->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                </div>
               
                <div class="geodir-category_contacts">
                    <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                    <ul class="no-list-style">
                        <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$bussiness->phone}}</a></li>
                        <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$bussiness->email}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
</div>
<!-- listing-item end --> 


@endforeach     

                                     @endif
                                    
                                    

                            

                                                                                       
                                    <!-- <div class="pagination fwmpag">
                                        <a href="#" class="prevposts-link"><i class="fas fa-caret-right"></i><span>قبلی</span></a>
                                        <a href="#">1</a>
                                        <a href="#" class="current-page">2</a>
                                        <a href="#">3</a>
                                        <a href="#">...</a>
                                        <a href="#">7</a>
                                        <a href="#" class="nextposts-link"><span>بعدی</span><i class="fas fa-caret-left"></i></a>
                                    </div> -->
                                </div>
                                <!-- listing-item-container end -->
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('scripts')


<script>

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
              

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
   
}

});

}
}

</script>


@endsection






