
<div class="clients_section wow fadeInUp">
    <div class="container">
        <h2 class="section_header elegant centered">مشتریان  </h2>
        <div class="clients_list">
            @foreach($customers as $customer)

            <a href="#">
                @if(!$customer->Hasmedia('images'))
                    <img src="{{asset('img/no-img.gif')}}" alt="client">

                @else
                    <img src="{{$customer->getFirstMediaUrl('images')}}" alt="client">

                @endif

            </a>

            @endforeach
        </div>
    </div>
</div>
