<!--about us start-->

<section class="content-property">
    <div class="container">
        <div class="row align-items-center content-property">
            <div class="col-lg-6 col-md-12">
                <div class="row">
                    <div class="col-md-6 pr-2">
                        <div class="about-img mb-3">
                            <img class="img-fluid" src="{{asset('template/images/about/02.jpg')}}" alt="">
                        </div>
                        <div class="about-img">
                            <img class="img-fluid" src="{{asset('template/images/about/01.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 mt-4 pl-2">
                        <div class="about-img mb-3">
                            <img class="img-fluid" src="{{asset('template/images/about/03.jpg')}}" alt="">
                        </div>
                        <div class="about-img">
                            <img class="img-fluid" src="{{asset('template/images/about/04.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 box-shadow white-bg px-4 py-4 sm-px-3 sm-py-3 xs-py-2 xs-px-2 md-mt-5">
                <h5>{{__('cms.features')}} </h5>
                <div class="row text-black mt-4">
                    <div class="col-sm-6">
                        <ul class="list-unstyled">
                            @foreach($properties as $property)
                                <li class="mb-2">- {{convert_lang($property,LaravelLocalization::getCurrentLocale(),'title')}} </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--about us end-->
