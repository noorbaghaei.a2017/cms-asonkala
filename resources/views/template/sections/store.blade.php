@if(\Modules\Core\Helper\CoreHelper::hasStore())
<div  id="section4">
    <section >
        <div class="container-fluid plp-container">
            <div class="plp-box">
                <strong class="section-title-inline-1">
                    <h1> Company </h1>
                </strong>
            </div>
        </div>
        <!-- Swiper -->
        <div class="swiper-container company-gallery-top">
            <div class="swiper-wrapper">
                @foreach($stores as $store)
                <div class="swiper-slide"
                     style="background-image:url({{$store->getFirstMediaUrl('images')}})"></div>
                @endforeach

            </div>

        </div>
        <div class="swiper-container company-gallery-thumbs">
            <div class="swiper-wrapper">
                @foreach($stores as $store)
                <div class="swiper-slide"
                     style="background-image:url({{$store->getFirstMediaUrl('images','thumb')}})"></div>
                @endforeach

            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-black" style="height: 30px"></div>
            <div class="swiper-button-prev swiper-button-black" style="height: 30px"></div>
        </div>
    </section>
</div>
@endif
