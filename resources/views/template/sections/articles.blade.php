<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link type="text/css" rel="stylesheet" href="{{asset('template/css/style.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">




<title>
ganzjob
</title>
</head>


<body>
<h5 id="time" style="text-align:center;">{{getDateTime()}} </h5>
<div class="wrapper">
	<div class="container" style="padding-top:0;">
		
	
		
	@if(checkDateHour())
		<form class="form" action="{{ route('member.login.submit') }}" method="POST" >
		@csrf
		@error('email')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
			<input name="identify" type="text" placeholder="E-mail" autocomplete="off">
			@error('password')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
			<input name="password" type="password" placeholder="Passwort" autocomplete="off">
			<button type="submit" >Anmelden</button>
		</form>
		@endif
		
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
<input hidden id="longitude" name="longitude" value="">
<input hidden id="latitude" name="latitude" value="">
</body>

<script src="{{asset('template/js/jquery.min.js')}}"></script>
<script src="{{asset('template/js/scripts.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<script src="https://kit.fontawesome.com/6405c887d4.js" crossorigin="anonymous"></script>

<script>
  if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                position => {
                  
                   
                      $("#longitude").attr('value',position.coords.longitude);
                      $("#latitude").attr('value',position.coords.latitude);
                    
                },
                error => {
                   
                    $("#longitude").attr('value','');
                      $("#latitude").attr('value','');
                     
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000});
        }

</script>


<!-- <script>

function updateTime(){
	var time="{{getDateTime()}}";
  $('#time').text(time);
  
}
$(function(){
  setInterval(updateTime, 1000);
});
</script> -->


</html>