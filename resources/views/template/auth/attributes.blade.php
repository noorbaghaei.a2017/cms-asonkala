@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                        
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.error')
                                       <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <form method="post" action="{{route('job.change.attribute',['token'=>$item->token])}}" class="custom-form">
                                    @csrf
                                   
                                         <!-- act-widget--> 
                                         <div class="act-widget fl-wrap">
                                         @foreach ($attributes as $value)
                                            <div class="act-widget-header">
                                                <h4>{{$value->title}}</h4>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="{{$value->id}}" class="onoffswitch-checkbox" id="{{$value->id}}"  {{in_array($value->id,$item_attributes) ? 'checked' : ''}} >
                                                    <label class="onoffswitch-label" for="{{$value->id}}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                            @if(count($attributes) > 0)
                                            <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>
                                            @endif
                                        </form>
                                        <!-- act-widget end--> 
                                    
                                       
                                                                                 
                                      
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




