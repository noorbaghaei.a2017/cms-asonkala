<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="csrf-token" content="{{ csrf_token() }}">
<link type="text/css" rel="stylesheet" href="{{asset('template/css/style.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">



<title>
ganzjob
</title>
</head>


<body style="height:100%;">
<div id="cover" style="background-color:#50a3a2;opacity:1">
<h5 id="time" style="text-align:center;">{{getDateTime()}} </h5>
<div class="wrapper">
	<div class="container" style="padding-top:0;">
	@if(auth('member')->check())
	<form action="{{route('member.logout')}}" method="GET" style="display:inline-block">
			
			<input type="submit" class="btn btn-danger" value="Abmelden" style="cursor:pointer">
			</form>

			@if(checkEndReport(auth('member')->user()->token))

			@else

				@if(checkNoFirstAction(auth('member')->user()->token))


						@if(checkEndReportBreak(auth('member')->user()->token) || checkNoReportBreak(auth('member')->user()->token))
							
								@if(checkDateHourToday(auth('member')->user()->token))
								<form action="{{route('start.attendance.break',['token'=>auth('member')->user()->token])}}" method="POST" style="display:inline-block">
								@csrf
								<input type="submit" class="btn btn-danger" value="Start Break" style="cursor:pointer">
								</form>

								@endif
						@else
								@if(checkDateHourToday(auth('member')->user()->token))
								<form action="{{route('end.attendance.break',['token'=>auth('member')->user()->token])}}" method="POST" style="display:inline-block">
									@csrf
									<input type="submit" class="btn btn-danger" value="End Break" style="cursor:pointer">
									</form>
									@endif
						@endif


				@endif


			@endif
		
			
			

	@endif
	
	
	
		
	 @if(checkDateHour())

			@if(!checkDateHourToday(auth('member')->user()->token))
					<form action="{{route('start.attendance.member',['token'=>auth('member')->user()->token])}}" method="POST">
					@csrf
					<input type="submit" class="btn btn-danger" value="Start" style="cursor:pointer">
					<input hidden id="longitude" name="longitude" value="">
					<input hidden id="latitude" name="latitude" value="">
					</form>
					
				

			@else
		
					@if(checkEndReport(auth('member')->user()->token))
						<h3>beendet deine Arbeit</h3>
					@else
					@if(checkEndReportBreak(auth('member')->user()->token) || checkNoReportBreak(auth('member')->user()->token))
					
					@if(checkDateHour())
					<form action="{{route('end.attendance.member',['token'=>auth('member')->user()->token])}}" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="file" name="image">
					<textarea  name="income" cols="30" rows="2" style="resize:none;margin-bottom:10px;padding:5px;"  placeholder="income">
					{{getCurrentReport(auth('member')->user()->token)->income}}
					</textarea>
					
					<textarea id="text_report"  name="text" cols="30" rows="4" style="resize:none;margin-bottom:10px;padding:5px;" placeholder="text" >
					{{getCurrentReport(auth('member')->user()->token)->text}}
					</textarea>
					<br>
					<input type="submit" class="btn btn-danger" value="Ende" style="cursor:pointer;">
					<input hidden id="longitude" name="longitude" value="">
					<input hidden id="latitude" name="latitude" value="">
					</form>
					@endif
					@endif

			@endif

		@endif


	@else

     <h3>Firma nicht eröffnet</h3>
	

	@endif
		
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	
	</ul>
</div>


</div>
</body>

<script src="{{asset('template/js/jquery.min.js')}}"></script>
<script src="{{asset('template/js/scripts.js')}}"></script>
<!-- <script src="{{asset('template/js/bootstrap.min.js')}}"></script> -->

<script>



  if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                position => {
                  
                   
                      $("#longitude").attr('value',position.coords.longitude);
                      $("#latitude").attr('value',position.coords.latitude);
                    
                },
                error => {
                   
                    $("#longitude").attr('value','');
                      $("#latitude").attr('value','');
                     
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000});
        }

		
		$("#text_report").keyup(function(e){
			console.log('ok');
			var text=e.target.value;

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/change/text/report/'+text,
    data: { field1:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
}

});


});
</script>


<!-- <script>

function updateTime(){
	var time="{{getDateTime()}}";
  $('#time').text(time);
  
}
$(function(){
  setInterval(updateTime, 1000);
});
</script> -->
<script>
$( "input[type='submit']" ).click(function(e) {
	
  $( "#cover" )
    .animate({ opacity: "0" }, 1000 )
  
});

</script>
</html>