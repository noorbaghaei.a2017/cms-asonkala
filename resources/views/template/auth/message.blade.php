@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">
                    <!--  section  -->
                    <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                          
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a class="add_new-dashboard">{{__('cms.reserve')}}   <i class="fal fa-layer-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           <div class="col-md-9">
                           @include('template.alert.error')
                            @include('template.alert.success')
                                <!-- dashboard-list-box-->         
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header color-bg fl-wrap">
                                        <h3>{{__('cms.message')}}</h3>
                                    </div>
                                    <div class="chat-wrapper fl-wrap">
                                        <div class="row">
                                            <!-- chat-box--> 
                                            <div class="col-sm-12">
                                  
                                            <form action="{{route('client.send.chat')}}" method="GET">
                                                <div class="chat-box fl-wrap">

                                                @foreach($chats as $chat)



                                                        @if($chat->user==null)

                                                              <!-- message--> 
                                                    <div class="chat-message chat-message_user fl-wrap">
                                                        <div class="dashboard-message-avatar">
                                                        @if(!$client->Hasmedia('images'))
                                                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                                                    
                                                        @else
                                                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                                                        @endif
                                                           
                                                            <span class="chat-message-user-name cmun_sm">you</span>
                                                        </div>
                                                        <span class="massage-date">{{$chat->created_at->ago()}}</span>
                                                        <p>{{$chat->text}}</p>
                                                    </div>
                                                    <!-- message end-->                                             
                                                                                                                                
                                                        @else


                                                <!-- message--> 
                                                <div class="chat-message chat-message_guest fl-wrap">
                                                        <div class="dashboard-message-avatar">
                                                            <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                                                            <span class="chat-message-user-name cmun_sm">iniaz</span>
                                                        </div>
                                                        <span class="massage-date">{{$chat->created_at->ago()}}  </span>
                                                        <p>{{$chat->text}}</p>
                                                    </div>
                                                    <!-- message end-->
                                                    @endif

                                                    @endforeach






                                                                  
                                                          
                                                    



                                                </div>
                                                <div class="chat-widget_input fl-wrap">
                                                    <textarea name="message" placeholder="{{__('cms.text')}}"></textarea>                                                 
                                                    <button type="submit"><i class="fal fa-paper-plane"></i></button>
                                                </div>
                                                </form>
                                            </div>
                                            <!-- chat-box end--> 
                                           
                                        </div>
                                    </div>
                                    <!-- dashboard-list-box end--> 
                                </div>
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


