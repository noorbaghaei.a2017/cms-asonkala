@extends('template.app')

@section('content')


@include('core::layout.alert-danger')

  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                            <div class="dashboard-breadcrumbs breadcrumbs"><a href="#">خانه</a><a href="#">داشبورد</a><span>صفحه اصلی</span></div>
                            <!--Tariff Plan menu-->
                            <div   class="tfp-btn"><span>طرح تعرفه : </span> <strong>تمدید شده</strong></div>
                            <div class="tfp-det">
                                <p>شما در حال <a href="#">تمدید هستید </a> . برای مشاهده جزئیات یا به روزرسانی از لینک زیر استفاده کنید. </p>
                                <a href="#" class="tfp-det-btn color2-bg">جزئیات</a>
                            </div>
                            <!--Tariff Plan menu end-->
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1>خوش آمدید  : <span>آلیسا نوری</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap">
                                    <div class="dashboard-header-avatar">
                                        <img src="images/avatar/4.jpg" alt="">
                                        <a href="dashboard-myprofile.html" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <!--  dashboard-header-stats-item -->
                                                    <div class="swiper-slide">
                                                        <div class="dashboard-header-stats-item">
                                                            <i class="fal fa-map-marked"></i>
                                                            تبلیغ های فعال  
                                                            <span>21</span>
                                                        </div>
                                                    </div>
                                                    <!--  dashboard-header-stats-item end -->
                                                    <!--  dashboard-header-stats-item -->
                                                    <div class="swiper-slide">
                                                        <div class="dashboard-header-stats-item">
                                                            <i class="fal fa-chart-bar"></i>
                                                            بازدید تبلیغ
                                                            <span>1054</span>   
                                                        </div>
                                                    </div>
                                                    <!--  dashboard-header-stats-item end -->
                                                    <!--  dashboard-header-stats-item -->
                                                    <div class="swiper-slide">
                                                        <div class="dashboard-header-stats-item">
                                                            <i class="fal fa-comments-alt"></i>
                                                            کل نظرات
                                                            <span>79</span> 
                                                        </div>
                                                    </div>
                                                    <!--  dashboard-header-stats-item end -->
                                                    <!--  dashboard-header-stats-item -->
                                                    <div class="swiper-slide">
                                                        <div class="dashboard-header-stats-item">
                                                            <i class="fal fa-heart"></i>
                                                            ذخیره شده
                                                            <span>654</span>    
                                                        </div>
                                                    </div>
                                                    <!--  dashboard-header-stats-item end -->
                                                </div>
                                            </div>
                                        </div>
                                        <!--  dashboard-header-stats  end -->
                                        <div class="dhs-controls">
                                            <div class="dhs dhs-prev"><i class="fal fa-angle-left"></i></div>
                                            <div class="dhs dhs-next"><i class="fal fa-angle-right"></i></div>
                                        </div>
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a class="add_new-dashboard">افزودن تبلیغ <i class="fal fa-layer-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            <!--  dashboard-menu-->
                            <div class="col-md-3">
                                <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> منوی داشبورد</div>
                                <div class="clearfix"></div>
                                <div class="fixed-bar fl-wrap" id="dash_menu">
                                    <div class="user-profile-menu-wrap fl-wrap block_box">
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>اصلی</h3>
                                            <ul class="no-list-style">
                                                <li><a href="dashboard.html"><i class="fal fa-chart-line"></i>داشبورد</a></li>
                                                <li><a href="dashboard-feed.html"><i class="fal fa-rss"></i>اطلاعیه <span>7</span></a></li>
                                                <li><a href="dashboard-myprofile.html" class="user-profile-act"><i class="fal fa-user-edit"></i> ویرایش پروفایل</a></li>
                                                <li><a href="dashboard-messages.html"><i class="fal fa-envelope"></i> پیام ها <span>3</span></a></li>
                                                <li><a href="dashboard-password.html"><i class="fal fa-key"></i>تغییر رمز عبور</a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>تبلیغ ها</h3>
                                            <ul  class="no-list-style">
                                                <li><a href="dashboard-listing-table.html"><i class="fal fa-th-list"></i> تبلیغ های من  </a></li>
                                                <li><a href="dashboard-bookings.html"> <i class="fal fa-calendar-check"></i> رزرو <span>2</span></a></li>
                                                <li><a href="dashboard-review.html"><i class="fal fa-comments-alt"></i> نظرات </a></li>
                                                <li><a href="dashboard-add-listing.html"><i class="fal fa-file-plus"></i> تبلیغ جدید</a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->                                        
                                        <button class="logout_btn color2-bg">خروج <i class="fas fa-sign-out"></i></button>
                                    </div>
                                </div>
                                <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu">بازگشت<i class="fas fa-caret-up"></i></a>
                            </div>
                            <!-- dashboard-menu  end-->
                            <!-- dashboard content-->
                            <div class="col-md-9">
                                <div class="dashboard-title fl-wrap">
                                    <h3>پروفایل شما</h3>
                                </div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>نام <i class="fal fa-user"></i></label>
                                                <input type="text" placeholder="آلیسا" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>نام خانوادگی <i class="fal fa-user"></i></label>
                                                <input type="text" placeholder="نوری" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>آدرس ایمیل<i class="far fa-envelope"></i>  </label>
                                                <input type="text" placeholder="Alisa15236@domain.com" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>تلفن<i class="far fa-phone"></i>  </label>
                                                <input type="text" placeholder="0211234567" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> آدرس <i class="fas fa-map-marker"></i>  </label>
                                                <input type="text" placeholder="ایران , تهران , زعفرانیه" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> سایت <i class="far fa-globe"></i>  </label>
                                                <input type="text" placeholder="rtl-theme.com" value=""/>                                                
                                            </div>
                                        </div>
                                        <label> یادداشت</label>                                              
                                        <textarea cols="40" rows="3" placeholder="درمورد من" style="margin-bottom:20px;"></textarea>
                                        <label>تغییر آواتار</label> 
                                        <div class="photoUpload">
                                            <span><i class="fal fa-image"></i> <strong>اضافه کردن عکس</strong></span>
                                            <input type="file" class="upload">
                                        </div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 
                                <div class="dashboard-title dt-inbox fl-wrap">
                                    <h3>شبکه اجتماعی شما</h3>
                                </div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <label>فیس بوک <i class="fab fa-facebook"></i></label>
                                        <input type="text" placeholder="https://www.facebook.com/" value=""/>
                                        <label>توییتر<i class="fab fa-twitter"></i>  </label>
                                        <input type="text" placeholder="https://twitter.com/" value=""/>
                                        <label>Vkontakte<i class="fab fa-vk"></i>  </label>
                                        <input type="text" placeholder="https://vk.com" value=""/>
                                        <label> اینستاگرام <i class="fab fa-instagram"></i>  </label>
                                        <input type="text" placeholder="https://www.instagram.com/" value=""/>
                                        <button class="btn    color2-bg  float-btn">ذخیره تغییرات<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->                                    
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection





