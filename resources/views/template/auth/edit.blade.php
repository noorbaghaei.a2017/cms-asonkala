@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                        
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                            <form action="{{route('foreign.client.update',['client'=>$client])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                    <div class="row">
                                    <div class="col-sm-4">
                                                <label>{{__('cms.first_name')}} <i class="fal fa-user"></i></label>
                                                <input type="text" class="{{$errors->has('first_name') ? 'error-input' : ''}}" name="first_name" placeholder="{{__('cms.first_name')}}" value="{{$client->first_name}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.last_name')}}  <i class="fal fa-user"></i></label>
                                               
                                                <input class="{{$errors->has('last_name') ? 'error-input' : ''}}" type="text" name="last_name"  placeholder="{{__('cms.last_name')}}" value="{{$client->last_name}}" autocomplete="off"/>                                                
 
                                            </div>
                                           
                                            <div class="col-sm-4">
                                                <label>{{__('cms.mobile')}}<i class="far fa-phone"></i>  </label>
                                                <input type="text" class="{{$errors->has('mobile') ? 'error-input' : ''}}" name="mobile" placeholder="{{__('cms.mobile')}}" value="{{$client->mobile}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.phone')}}<i class="far fa-phone"></i>  </label>
                                                <input type="text" class="{{$errors->has('phone') ? 'error-input' : ''}}"  name="phone" placeholder="{{__('cms.phone')}}" value="{{$client->phone}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.email')}} <i class="far fa-envelope"></i>  </label>
                                                <input type="text" class="{{$errors->has('email') ? 'error-input' : ''}}" name="email" placeholder="{{__('cms.email')}}" value="{{$client->email}}" autocomplete="off"/>                                                
                                            </div>

                                    </div>
                                        <div class="row">
                                         
                                            <div class="col-sm-3">
                                                <label> {{__('cms.address')}} <i class="fas fa-map-marker"></i>  </label>
                                               
                                                <input type="text" class="{{$errors->has('address') ? 'error-input' : ''}}"  name="address" placeholder="{{__('cms.address')}}" value="{{$client->address}}" autocomplete="off"/>                                                
                  
                                            </div>
                                            <div class="col-sm-3">
                                                <label> {{__('cms.postal_code')}} <i class="far fa-globe"></i>  </label>
                                                <input id="postal_code" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" onkeyup="loadCity()" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{$client->postal_code}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> {{__('cms.city')}} <i class="far fa-globe"></i>  </label>
                                                <select  id="city" class="chosen-select no-search-select {{$errors->has('city') ? 'error-input' : ''}}" type="text" name="city">

                                                    @if(!is_null($client->city))
                                                    <option value="{{$client->city}}" selected>{{$client->city}}</option>
                                                    @endif
                                                </select>                                                
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="row">

<!--col --> 
<div class="col-md-5">
       
       <div class="add-list-media-wrap">
              @csrf
                      @if(!$client->Hasmedia('images'))
                      <div id="bg" class="fuzone">
                   <div id="text-image" class="fu-text">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                        @else

                        <div id="bg" class="fuzone" style="background:url({{$client->getFirstMediaUrl('images')}});background-size:cover">
                   <div id="text-image" class="fu-text" style="display:none">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                       
                        @endif
               

         
       </div>
   </div>
   <!--col end--> 

</div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 

                                
                                
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <label> facebook <i class="fab fa-facebook"></i></label>
                                        <input type="text" name="facebook" placeholder="https://www.facebook.com/" value=""/>
                                        <label>twitter<i class="fab fa-twitter"></i>  </label>
                                        <input type="text" name="twitter" placeholder="https://twitter.com/" value=""/>
                                        <label> instagram <i class="fab fa-instagram"></i>  </label>
                                        <input type="text" name="instagram" placeholder="https://www.instagram.com/" value=""/>
                                        <label> {{__('cms.website')}} <i class="fab fa-chrome"></i>  </label>
                                        <input type="text" name="website" placeholder="https://www.example.com/" value=""/>
                                        <label> youtube <i class="fab fa-youtube"></i>  </label>
                                        <input type="text" name="youtube" placeholder="https://www.youtube.com/" value=""/>
                                        <label> github <i class="fab fa-github"></i>  </label>
                                        <input type="text" name="github" placeholder="https://www.github.com/" value=""/>
                                        <label> telegram <i class="fab fa-telegram"></i>  </label>
                                        <input type="text" name="telegram" placeholder="https://www.telegram.com/" value=""/>
                                        <label> whatsapp <i class="fab fa-whatsapp"></i>  </label>
                                        <input type="text" name="whatsapp" placeholder="https://www.whatsapp.com/" value=""/>
                                        <label>pinterest <i class="fab fa-pinterest"></i>  </label>
                                        <input type="text" name="pinterest" placeholder="https://www.pinterest.com/" value=""/>

                                        <button class="btn    color2-bg  float-btn">{{__('cms.update')}} <i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->  

                                </form>                                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
              

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
   
}

});

}
}

</script>


@endsection




