<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>
ganzjob
</title>
</head>


<body>
<link href="{{asset('template/css/style.css')}}" rel="stylesheet">

<link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<script src="{{asset('template/js/jquery.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<script src="{{asset('template/js/ckeditor.js')}}"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-lg-12">
           
            <div class="account-wall">
			<h1 id="time" style="text-align:center;">{{getDateTime()}} </h1>
			@if(auth('member')->check())
      @if(checkDateHour())
						<div class="row">

<div class="col-lg-12">
<form class="form-signin" action="{{route('member.logout')}}" method="GET" >
						@csrf
						<input type="submit" class="btn btn-primary btn-lg btn-block" value="Abmelden" style="cursor:pointer">

					
						</form>

</div>


						</div>

            @else
             <h3 class="text-center">Firma nicht eröffnet</h3>

						@endif
            @endif
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">
				
	
		
		
		<!------ start check time system ---------->
	

	 
		
				
		@if(auth('member')->check())

    @if(checkDateHour())
		<div class="row">

    <div class="col-lg-12">

        @if(checkEndReport(auth('member')->user()->token))

        @else

              @if(checkNoFirstAction(auth('member')->user()->token))


                  @if(checkEndReportBreak(auth('member')->user()->token) || checkNoReportBreak(auth('member')->user()->token))
                    
                      @if(checkDateHourToday(auth('member')->user()->token))
                      <form action="{{route('start.attendance.break',['token'=>auth('member')->user()->token])}}" method="POST" style="display:inline-block">
                      @csrf
                      <input type="submit" class="btn btn-primary btn-lg btn-block" value="Start Break" style="cursor:pointer">
                      </form>

                      @endif
                  @else
                      @if(checkDateHourToday(auth('member')->user()->token))
                      <form action="{{route('end.attendance.break',['token'=>auth('member')->user()->token])}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="End Break" style="cursor:pointer">
                        </form>
                        @endif
                  @endif


          @endif


    @endif


  @if(auth('member')->check())
    @if(!checkDateHourToday(auth('member')->user()->token))
					<form action="{{route('start.attendance.member',['token'=>auth('member')->user()->token])}}" method="POST">
					@csrf
					<input type="submit" class="btn btn-primary btn-lg btn-block" value="Start" style="cursor:pointer">
					<input hidden id="longitude" name="longitude" value="">
					<input hidden id="latitude" name="latitude" value="">
					</form>
					
				

			@else


              @if(checkEndReport(auth('member')->user()->token))
                <h3 class="text-center">beendet deine Arbeit</h3>
              @else

                @if(checkEndReportBreak(auth('member')->user()->token) || checkNoReportBreak(auth('member')->user()->token))
              



                          <form action="{{route('end.attendance.member',['token'=>auth('member')->user()->token])}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          <input type="file" name="image">
                          <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Text</a></li>
                        <li><a data-toggle="tab" href="#menu1">Hülle</a></li>
                        <li><a data-toggle="tab" href="#menu2">Tasche</a></li>
                        <li><a data-toggle="tab" href="#menu3">Glas</a></li>
                        <li><a data-toggle="tab" href="#menu4">Zubehhör</a></li>
                        <li><a data-toggle="tab" href="#menu5">Kasse</a></li>
                        <li><a data-toggle="tab" href="#menu6">Karte</a></li>
                        <li><a data-toggle="tab" href="#menu7">Bar</a></li>

                      </ul>

                      <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                        <textarea id="text_report"  name="text" cols="55" rows="5" style="resize:none;" placeholder="text" >{!!getCurrentReport(auth('member')->user()->token)->text!!}</textarea>
                      
                        </div>
                       

                        <div id="menu1" class="tab-pane fade">
                        
                        <textarea id="hulle_report"  name="hulle" cols="55" rows="5" style="resize:none"  placeholder="hulle">{!!getCurrentReport(auth('member')->user()->token)->hulle!!}</textarea>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                        
                        <textarea id="tasche_report"  name="tasche" cols="55" rows="5" style="resize:none"  placeholder="tasche">{!!getCurrentReport(auth('member')->user()->token)->tasche!!}</textarea>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                      
                        <textarea id="glas_report"  name="glas" cols="55" rows="5" style="resize:none"  placeholder="glas">{!!getCurrentReport(auth('member')->user()->token)->glas!!}</textarea>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                        
                            <textarea id="zubehhor_report"  name="zubehhor" cols="55" rows="5" style="resize:none"  placeholder="zubehhor">{!!getCurrentReport(auth('member')->user()->token)->zubehhor!!}</textarea>
                        </div>
                        <div id="menu5" class="tab-pane fade">
                        
                        <input type="text" id="kasse"  name="kasse" class="form-control"    placeholder="kasse ist Zum Beispiel (12 Euro 9 cent) :  12.9">
                    </div>
                    <div id="menu6" class="tab-pane fade">
                        
                        <input type="text" id="karte"  name="karte" class="form-control"    placeholder="karte ist Zum Beispiel (12 Euro 9 cent) : 12.9">
                    </div>
                    <div id="menu7" class="tab-pane fade">
                        
                        <input type="text" id="bar"  name="bar" class="form-control"   placeholder="bar ist Zum Beispiel (12 Euro 9 cent) : 12.9">
                    </div>
                      
                      </div>
<br>
<br>
<br>
<br>
<input hidden id="longitude" name="longitude" value="">
                          <input hidden id="latitude" name="latitude" value="">
<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#myModal">Fertig</button>
                             
                                                     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Achtung</h4>
        </div>
        <div class="modal-body">
          <p>Bist du dir sicher, dass du deinen Job beendest??</p>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-warning" data-dismiss="modal">Nein</a>
         
           <input type="submit" class="btn btn-danger " value="Ja" style="cursor:pointer;">
        </div>
       
      </div>
    </div>
  </div>
                        
                          </form>


                   @endif
               @endif             

     @endif
     @endif
  @endif            
		</div>

		</div>
		

		@endif
		

		


	

    
	



		<!------ end check time system ---------->

				
            </div>
         
        </div>
    </div>
</div>




<script>
  if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                position => {
                  
                   
                      $("#longitude").attr('value',position.coords.longitude);
                      $("#latitude").attr('value',position.coords.latitude);
                    
                },
                error => {
                   
                    $("#longitude").attr('value','');
                      $("#latitude").attr('value','');
                     
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000});
        }







$("#income_report").keyup(function(e){
			console.log('ok');
			var income=e.target.value;

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/change/income/report/'+income,
    data: { field1:income} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
}

});

});






 
$('input[name=bar]').keyup(function(){
   
        $(this).val($(this).val().replace(/[^\d.]/,''));
    });

    $('input[name=karte]').keyup(function(){
   
   $(this).val($(this).val().replace(/[^\d.]/,''));
});

$('input[name=kasse]').keyup(function(){
   
   $(this).val($(this).val().replace(/[^\d.]/,''));
});


CKEDITOR.replace( 'text_report', {
	uiColor: '##337ab7',
	toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor' ]
	]
}).on('key',
    function(e){
        setTimeout(function () {
        console.log('ok');
			var text=e.editor.getData();

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    dataType: "json",
    url:'/ajax/change/text/report',
    data: { text:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
        }

        });
    }, 1000);
            }
);









CKEDITOR.replace( 'tasche_report', {
	uiColor: '##337ab7',
    language: 'de',
	toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor' ]
	]
}).on('key',
    function(e){
        setTimeout(function () {
        console.log('ok');
			var text=e.editor.getData();

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    dataType: "json",
    url:'/ajax/change/tasche/report',
    data: { tasche:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
        }

        });
    }, 1000);
            }
);











CKEDITOR.replace( 'hulle_report', {
	uiColor: '##337ab7',
    language: 'de',
	toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor' ]
	]
}).on('key',
    function(e){
        setTimeout(function () {
        console.log('ok');
			var text=e.editor.getData();

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    dataType: "json",
    url:'/ajax/change/hulle/report',
    data: { hulle:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
        }

        });
    }, 1000);
            }
);








CKEDITOR.replace( 'glas_report', {
	uiColor: '##337ab7',
    language: 'de',
	toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor' ]
	]
}).on('key',
    function(e){
        setTimeout(function () {
        console.log('ok');
			var text=e.editor.getData();

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    dataType: "json",
    url:'/ajax/change/glas/report',
    data: { glas:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
        }

        });
    }, 1000);
            }
);









CKEDITOR.replace( 'zubehhor_report', {
	uiColor: '##337ab7',
    language: 'de',
	toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor' ]
	]
}).on('key',
    function(e){
        setTimeout(function () {
        console.log('ok');
			var text=e.editor.getData();

			$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    dataType: "json",
    url:'/ajax/change/zubehhor/report',
    data: { zubehhor:text} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
       
       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
 
    },
    complete:function(){
   
        }

        });
    }, 1000);
            }
);


</script>



</body>

</html>