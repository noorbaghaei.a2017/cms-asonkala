
@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                          
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                            <form action="{{route('client.store.bussiness',['client'=>$client])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                  
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                    <div class="row">
                                                <div class="col-sm-12">
                                                <label>{{__('cms.title')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12">
                                                <label>{{__('cms.excerpt')}}  <i class="fal fa-text"></i></label>
                                                <input type="text" name="excerpt" class="{{$errors->has('excerpt') ? 'error-input' : ''}}" placeholder="{{__('cms.excerpt')}}" value="{{old('excerpt')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12" style="margin-bottom:15px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                                <textarea type="text" name="text" class="{{$errors->has('text') ? 'error-input' : ''}}" placeholder="{{__('cms.text')}}" >
                                                {{old('text')}}
                                                </textarea>                                                
                                            </div>

                                          
                                            <div class="col-sm-6">
                                             <label> {{__('cms.category')}} <i class="far fa-globe"></i>  </label>
                                             <select  class="chosen-select no-search-select {{$errors->has('category') ? 'error-input' : ''}}" type="text" name="category">

                                             @foreach ($all_category_bussiness as $category)
                                                 
                                             <option value="{{$category->id}}">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                             
                                             @endforeach


                                             </select>                                                
                                             </div>
                                             <div class="col-sm-6">
                                                <label>{{__('cms.email')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}"  placeholder="{{__('cms.email')}}" value="{{old('email')}}" autocomplete="off"/>                                                
                                            </div>
</div>
                                           <div style="margin-bottom:15px"></div>
                                           <div class="row">
                                          
                                            <div class="col-sm-4">
                                                <label>{{__('cms.phone')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="phone" class="{{$errors->has('phone') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.phone-2')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="phone-2" class="{{$errors->has('phone-2') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone-2')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.mobile')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="mobile" class="{{$errors->has('mobile') ? 'error-input' : ''}}"  placeholder="{{__('cms.mobile')}}" value="{{old('mobile')}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                         
                                    </div>

                                    <div class="row">
                                         
                                         <div class="col-sm-4">
                                             <label> {{__('cms.address')}} <i class="fas fa-map-marker"></i>  </label>
                                             <input type="text" name="address" class="{{$errors->has('address') ? 'error-input' : ''}}" placeholder="{{__('cms.address')}}" value="{{old('address')}}" autocomplete="off"/>                                                
                                         </div>
                                         <div class="col-sm-4">
                                             <label> {{__('cms.postal_code')}} <i class="far fa-globe"></i>  </label>
                                             @if(!is_null(mostJobBot(auth('client')->user()->id)))
                                             <input id="postal_code" onkeyup="loadCity(event)" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->postal_code}}" autocomplete="off"/>                                                

                                             @else
                                             <input id="postal_code" onkeyup="loadCity(event)" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{old('postal_code')}}" autocomplete="off"/>                                                


                                             @endif
                                         </div>
                                         <div class="col-sm-4">
                                         <input hidden id="longitude" name="longitude" type="text" value="{{old('longitude')}}">
                                         <input hidden id="latitude" name="latitude" type="text" value="{{old('latitude')}}">
                                             <label> {{__('cms.city')}} <i class="far fa-globe"></i>  </label>
                                           
                                             <select id="city" class="chosen-select no-search-select {{$errors->has('city') ? 'error-input' : ''}}" type="text" name="city" >

                                             @if(!empty(old('city')))
                                             <option value="{{old('city')}}" selected>{{old('city')}}</option>
                                             
                                             @else

                                             @if(!is_null(mostJobBot(auth('client')->user()->id)))

                                             <option value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}" selected>{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}</option>

                                             @else
                                             <option value="" selected></option>

                                             @endif
                                             @endif

                                             </select>                                                
                                         </div>
                                        
                                     </div>
                                       
                                       
                                        <div class="row">

                                        <!--col --> 
                                        <div class="col-md-4">
                                               
                                               <div class="add-list-media-wrap">
                                                      @csrf
                                                       <div id="bg" class="fuzone">
                                                           <div id="text-image" class="fu-text">
                                                               <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                           </div>
                                                               <input type="file" name="image" class="upload" onchange="loadFile(event)">
                                                              

                                                       </div>

                                                 
                                               </div>
                                           </div>
                                           <!--col end--> 

                                        </div>
                                       
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 

                                
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Longitude (Drag marker on the map)<i class="fal fa-long-arrow-alt-right"></i>  </label>
                                                <input type="text" placeholder="Map Longitude"  id="long" value=""/>                                                
                                            </div>
                                            <div class="col-md-6">
                                                <label>Latitude (Drag marker on the map) <i class="fal fa-long-arrow-alt-down"></i> </label>
                                                <input type="text" placeholder="Map Latitude"  id="lat" value=""/>                                            
                                            </div>
                                        </div>
                                        <div class="map-container">
                                            <div id="singleMap" class="drag-map" data-latitude="52.52573" data-longitude="13.42608"></div>
                                        </div>
                                      
                                      
                                        <button class="btn    color2-bg  float-btn">{{__('cms.send')}} <i class="fal fa-save"></i></button>

                                    </div>
                                </div>
                                <!-- profile-edit-container end-->   
                              

                                </form>                                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){



console.log($('#latitude').val());
          

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
    
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
              

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
   
}

});

}
}

</script>


@endsection




