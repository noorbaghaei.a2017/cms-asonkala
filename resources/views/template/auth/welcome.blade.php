<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif



    <link href="{{asset('template/user/css/bootstrap-rtl.css')}}" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>{{__(__('cms.welcome'))}}   </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />

    @if(LaravelLocalization::getCurrentLocale()==="fa" || LaravelLocalization::getCurrentLocale()==="ar")
        <link href="{{asset('template/user/css/login.css')}}" rel="stylesheet" />


    @else
        <link href="{{asset('template/user/css/ltr-login.css')}}" rel="stylesheet" />
    @endif

    <style>

        .theme-btn{
            text-decoration: none;
            position: relative;
            display: inline-block;
            font-size: 13px;
            line-height: 30px;
            color: #e1e0de;
            padding: 7px 10px;
            margin: 15px;
            font-weight: 500;
            overflow: hidden;
            border-radius: 50px;
            background-color: #363a40;
            text-transform: capitalize;
            box-shadow: 0px 2px 15px rgb(0 0 0 / 5%);
            font-family: iransans-medium;
        }
    </style>
</head>

<body>

<div class="container">
    <div class="info">
        <h1>{{__('cms.welcome')}} </h1>
    </div>
</div>
<div class="form" style="margin-bottom: 0">
    <div>
        <a href="{{route('front.website')}}">

            @if(!$setting->Hasmedia('logo'))
                <img src="{{asset('img/no-img.gif')}}" style="width: 150px;"/>
            @else
                <img src="{{$setting->getFirstMediaUrl('logo')}}" style="width:150px;"/>
            @endif

        </a>

    </div>
      <div style="display: flex;justify-content: space-between">
          <a href="{{route('client.dashboard')}}" class="theme-btn">{{__('cms.complete-account')}}</a>
          <a href="{{route('front.website')}}" class="theme-btn">{{__('cms.return-website')}}</a>
      </div>

</div>

<div class="form" style="margin-top: 0;background: #dbb16b">
    <h5>{{__('cms.note')}} : </h5>
    <p>
        {{__('cms.confirm-after-login')}}
    </p>
</div>




</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/jquery-3.6.0.min.js')}}" type="text/javascript"></script>

<script src="{{asset('template/user/js/login.js')}}" type="text/javascript"></script>


</html>

