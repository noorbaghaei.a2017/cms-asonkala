<table>
    <thead>
    <tr>
        <th>date</th>
        <th>branch</th>
        <th>email</th>
        <th>text</th>
        <th>income</th>
        <th>start</th>
        <th>end</th>
        <th>result</th>
        <th>break</th>
    </tr>
    </thead>
    <tbody>
    @foreach($member_report_collection as $report)
        <tr>
             <td>{{ explode('-',$report->start)[0]}}-{{ explode('-',$report->start)[1]}}-{{ explode(' ',explode('-',$report->start)[2])[0]}}</td>
             <td>{{ $report->Branch }}</td>
            <td>{{ $report->Email }}</td>
            <td>{{ explode(' ',explode('-',$report->start)[2])[1]}}</td>
            @if (!is_null($report->end))
            <td>{{ explode(' ',explode('-',$report->end)[2])[1]}}</td>

            @else
            <td></td>

            @endif
            @if (!is_null($report->end))
            <td>{{calcTimeReportHour($report->start,$report->end)}}:{{calcTimeReportMinu($report->start,$report->end)}}</td>
           @else
           <td></td>
           @endif
           @if(!is_null($report->end) && (count($report->member_report_break()->get())))
           <td>{{calcTimeBreakReportHour($report->token)}}:{{calcTimeBreakReportMinu($report->token)}}</td>
           @else
           <td>00:00</td>
           @endif
        </tr>
    @endforeach
    </tbody>
</table>
