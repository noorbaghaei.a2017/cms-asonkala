@extends('template.app')

@section('content')


 <!-- content-->
 <div class="content">
                    <div class="page-scroll-nav">
                        <nav class="scroll-init color2-bg">
                            <ul class="no-list-style">
                                <li><a class="act-scrlink tolt" href="#sec1" data-microtip-position="left" data-tooltip=" {{__('cms.about-us')}}"><i class="fal fa-building"></i></a></li>
                                <li><a href="#sec2" class="tolt" data-microtip-position="left" data-tooltip="فیلم تبلیغاتی"><i class="fal fa-video"></i></a></li>
                                <li><a href="#sec3" class="tolt" data-microtip-position="left" data-tooltip=" {{__('cms.staff')}}"><i class="far fa-users-crown"></i></a></li>
                                <li><a href="#sec4" class="tolt" data-microtip-position="left" data-tooltip="چرا ما"><i class="fal fa-user-astronaut"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/30.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.about-us')}}</span></h2>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                         
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="list-single-main-media fl-wrap" style="box-shadow: 0 9px 26px rgba(58, 87, 135, 0.2);">
                                            <img src="{{asset('template/images/all/55.jpg')}}" class="respimg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="ab_text">
                                            <div class="ab_text-title fl-wrap">
                                                <h3>{{__('cms.about-us')}}</span></h3>
                                              
                                                <span class="section-separator fl-sec-sep"></span>
                                            </div>
                                            <p>
text about us
                                            </p>
                                            <a href="#sec3" class="btn color2-bg float-btn custom-scroll-link"> {{__('cms.staff')}} <i class="fal fa-users"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                            <span class="fw-separator"></span>
                           
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg particles-wrapper">
                        <div class="container">
                            <div class="section-title">
                                <h2>  {{__('cms.properties')}}</h2>
                              
                            </div>
                            <div class="process-wrap_time-line fl-wrap">
                                 <!--process-item-->
                                 <div class="process-item_time-line">
                                    <div class="pi_head color-bg">3</div>
                                    <div class="pi-text fl-wrap">
                                        <h4> target</h4>
                                       <p>
                                       text target
                                       </p>
                                    </div>
                                </div>
                                <!--process-item end-->    
                                 <!--process-item-->
                                 <div class="process-item_time-line">
                                    <div class="pi_head color-bg">3</div>
                                    <div class="pi-text fl-wrap">
                                        <h4> target</h4>
                                       <p>
                                       text target
                                       </p>
                                    </div>
                                </div>
                                <!--process-item end-->    
                                <!--process-item-->
                                <div class="process-item_time-line">
                                    <div class="pi_head color-bg">3</div>
                                    <div class="pi-text fl-wrap">
                                        <h4> target</h4>
                                       <p>
                                       text target
                                       </p>
                                    </div>
                                </div>
                                <!--process-item end-->                                                            
                            </div>
                            <a href="#" class="btn color2-bg">{{__('cms.read_more')}}  <i class="fal fa-angle-right"></i></a>
                        </div>
                        <div id="particles-js" class="particles-js"></div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="parallax-section video-section" data-scrollax-parent="true" id="sec2">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/34.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="video_section-title fl-wrap">
                                <h4>{{__('cms.text')}}</h4>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section -->  
                    <section id="sec3">
                        <div class="container">
                            <div class="section-title">
                                <h2> {{__('cms.staff')}} </h2>
                               
                             
                            </div>
                            <div class="about-wrap team-box2 fl-wrap">
                            @foreach ($members as $member)
                                 <!-- team-item -->
                                 <div class="team-box">
                                    <div class="team-photo">
                                    @if(!$member->Hasmedia('images'))
                                                                       
                                                                        <img src="{{asset('img/no-img.gif')}}" alt="" class="respimg">
                                                                    @else
                                                                   
                                                                    <img src="{{$member->getFirstMediaUrl('images')}}" alt="" class="respimg">
                                                                    @endif
                                        
                                    </div>
                                    <div class="team-info fl-wrap">
                                        <h3><a href="#"> {{$member->full_name}} </a></h3>
                                        <h4>{{$member->role_name}} </h4>
                                        <p>
                                       
                                        </p>
                                        <div class="team-social">
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- team-item  end-->
                            @endforeach
                               
                               
                            </div>
                        </div>
                        <div class="waveWrapper waveAnimation">
                          <div class="waveWrapperInner bgMiddle">
                            <div class="wave-bg-anim waveMiddle" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                          <div class="waveWrapperInner bgBottom">
                            <div class="wave-bg-anim waveBottom" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                        </div> 						
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg">
                        <div class="container">
                          
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="parallax-section" data-scrollax-parent="true" id="sec4">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/33.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.features')}}</span></h2>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg absolute-wrap_section">
                        <div class="container">
                            <div class="absolute-wrap fl-wrap">
                                <!-- features-box-container --> 
                                <div class="features-box-container fl-wrap">
                                    <div class="row">
                                         <!--features-box --> 
                                         <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-users-cog"></i>
                                                </div>
                                                <h3>service </h3>
                                             <p>
text service
                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-users-cog"></i>
                                                </div>
                                                <h3>service </h3>
                                             <p>
text service
                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-users-cog"></i>
                                                </div>
                                                <h3>service </h3>
                                             <p>
text service
                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                    </div>
                                </div>
                                <!-- features-box-container end  -->                             
                            </div>
                            <div class="section-separator"></div>
                        </div>
                    </section>
                    <!--section end-->  
                  
                </div>
                <!--content end-->




@endsection
