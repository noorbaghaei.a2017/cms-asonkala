@extends('template.app')

@section('content')


    <!-- Page Title -->
    <section class="page-title">
        <div class="outer-container">
            <div class="image">
                <img src="{{asset('template/images/background/20.jpg')}}" alt="" />
            </div>
        </div>
    </section>
    <!-- End Page Title -->

    <!-- Page Breadcrumb -->
    <section class="page-breadcrumb">
        <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
        <div class="container">
            <div class="clearfix">
                <div class="pull-right">

                </div>
                <div class="pull-left">

                </div>
            </div>
        </div>
    </section>
    <!-- End Page Breadcrumb -->

    <!-- Doctor Single Section -->
    <section class="doctor-detail-section">
        <div class="container">
            <div class="row">



                <!-- Content Column -->
                <div class="content-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <h2>اطلاعات شخصی</h2>
                        <ul class="doctor-info-list">
                            <li><span>نام پزشک</span>سپهر جم</li>
                            <li><span>تخصص اصلی</span>کلیه</li>
                            <li><span>تجربه</span>9+ سال</li>
                        </ul>
                        <h2>تحصیلات</h2>
                        <ul class="doctor-info-list">
                            <li><span>رشته پزشکی</span>دانشگاه شهید بهشتی</li>
                            <li><span>رزیدنتی</span>بیمارستان آسمان</li>
                            <li><span>تخصص داخلی</span>دانشگاه شهدا</li>
                            <li><span>تخصص کلیه</span>دانشگاه استنفرد آمریکا</li>
                        </ul>
                        <h2>شبکه‌های اجتماعی</h2>
                        <!-- Social Box -->
                        <ul class="social-box">
                            <li class="facebook"><a href="#"><span class="icon icon icon-facebook"></span></a></li>
                            <li class="twitter"><a href="#"><span class="icon icon icon-twitter"></span></a></li>
                            <li class="linkedin"><a href="#"><span class="icon icon icon-linkedin"></span></a></li>
                            <li class="youtube"><a href="#"><span class="icon icon icon-youtube"></span></a></li>

                        </ul>
                    </div>
                </div>
                <!-- Image Column -->
                <div class="image-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="{{asset('template/images/resource/doctor.jpg')}}" alt="" />
                            <div class="number-box">
                                <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="play-button" data-fancybox=""
                                   data-caption=""><i class="ripple"></i><i class="icon flaticon-video"></i></a>
                                212-243-7969
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Page Section -->

    <!-- Biography Section -->
    <div class="biography-section">
        <div class="pattern-one" style="background-image: url({{asset('template/images/icons/pattern-icon-10.png')}});"></div>
        <div class="container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-8 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <p>و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای
                            زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها
                            شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                        </p>
                        <p>و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای
                            زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها
                            شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                        </p>
                        <div class="signature">
                            <img src="{{asset('template/images/icons/signature.png')}}" alt="" />
                        </div>
                    </div>
                </div>
                <!-- Title Column -->
                <div class="title-column col-lg-4 col-md-12 col-sm-12">
                    <h2>بیوگرافی</h2>
                </div>



            </div>
        </div>
    </div>
    <!-- End Biography Section -->

    <!-- Experiance Section -->
    <section class="experiance-section">
        <div class="pattern-layer" style="background-image:url({{asset('templat/images/background/pattern-10.png')}})"></div>
        <div class="pattern-layer-two" style="background-image:url({{asset('template/images/background/pattern-11.png')}})"></div>
        <div class="container">
            <div class="row">

                <!-- Skills Column -->
                <div class="skills-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column" style="background-image: url({{asset('template/images/resource/skills.jpg')}});">
                        <h3>تجارب</h3>
                        <!--Skills-->
                        <div class="skills">

                            <!--Skill Item-->
                            <div class="skill-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">کلیه</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner">
                                        <div class="bar progress-line" data-width="85">
                                            <div class="skill-percentage">
                                                <div class="count-box"><span class="count-text" data-speed="2000" data-stop="85">0</span>%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="skill-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">داخلی</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner">
                                        <div class="bar progress-line" data-width="93">
                                            <div class="skill-percentage">
                                                <div class="count-box"><span class="count-text" data-speed="2000" data-stop="93">0</span>%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="skill-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">عمومی</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner">
                                        <div class="bar progress-line" data-width="95">
                                            <div class="skill-percentage">
                                                <div class="count-box"><span class="count-text" data-speed="2000" data-stop="95">0</span>%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="skill-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">جراحی</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner">
                                        <div class="bar progress-line" data-width="98">
                                            <div class="skill-percentage">
                                                <div class="count-box"><span class="count-text" data-speed="2000" data-stop="98">0</span>%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <!-- Featured Column -->
                <div class="featured-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">

                        <ul class="featured-list-two">
                            <li class="wow fadeInUp clearfix" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <span class="icon icon-brifecase-hospital2"></span>
                                <div class="content">
                                    <div class="title">مراقبت های ویژه</div>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطر </p>
                                </div>
                            </li>
                            <li class="wow fadeInUp clearfix" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <span class="icon icon-doctor"></span>
                                <div class="content">
                                    <div class="title">مراقبت های ویژه</div>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطر و غیره است.</p>
                                </div>
                            </li>
                            <li class="wow fadeInUp clearfix" data-wow-delay="600ms" data-wow-duration="1500ms">
                                <span class="icon icon-heart1"></span>
                                <div class="content">
                                    <div class="title">مراقبت های ویژه</div>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطر و غیره است.</p>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
    </section>





@endsection
