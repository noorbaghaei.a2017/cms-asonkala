@extends('template.app')

@section('content')


<section class="ads container" style="margin-top: 10px;margin-bottom: 10px;">


<div class="row">


    <div class="col-lg-7 col-md-4 col-sm-12">

        <div >
        @if(!$item->Hasmedia('images'))
                                <img src="{{asset('template/images/job.jpeg')}}" alt="" class="img-responsive thumb-job" style="margin-top: 15px;"/>
                            @else
                                <img src="{{$item->getFirstMediaUrl('images')}}" alt=""  class="img-responsive thumb-job" style="margin-top: 15px;"/>
                            @endif


        
        </div>
       

    </div>

    <div class="col-lg-5 col-md-4 col-sm-12">

        <div class="item">

        <h1>{{$item->title}}</h1>
        <p>

        {{$item->excerpt}}
    </p>
        </div>
       

    </div>
   

   

</div>


</section>

<section>


<h1 class="text-center">
    Menu
</h1>


<div class="text-center" style="margin-bottom:15px">
    <ul class="nav nav-tabs" style="display: inline-block;">
        <li class="active"><a data-toggle="tab" href="#home"><span class="fa fa-bars"></span></a></li>
        <li><a data-toggle="tab" href="#menu1"><span class="fa fa-glass"></span></a></li>
    
      </ul>
      
      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="info-ad container">
            @foreach(getField($item,'food') as $drink)
                <div class="col-lg-4">
        
                    <div class="row">
            
                        <div class="col-lg-6">
                        @if(!$drink->Hasmedia('images'))
                                <img src="{{asset('template/images/job.jpeg')}}" alt="" class="img-responsive thumb-job"/>
                            @else
                                <img src="{{$drink->getFirstMediaUrl('images')}}" alt=""  class="img-responsive thumb-job"/>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <h5>{{$drink->title}}</h1>
                            <p>{{$drink->excerpt}}</p>
                            <span>{{$drink->price}}$</span>
                        </div>
                        
                        
                    </div>
            
                </div>
                @endforeach
                
       
            </div>
        </div>
        <div id="menu1" class="tab-pane fade">

            




<div class="row">


<div class="info-ad container">
    
@foreach(getField($item,'drink') as $food)

<div class="col-lg-4">
        
<div class="row">

    <div class="col-lg-6">
                            @if(!$food->Hasmedia('images'))
                                <img src="{{asset('template/images/job.jpeg')}}" alt="" class="img-responsive thumb-job"/>
                            @else
                                <img src="{{$food->getFirstMediaUrl('images')}}" alt=""  class="img-responsive thumb-job"/>
                            @endif
    </div>
    <div class="col-lg-6">
        <h5>{{$food->title}}</h1>
        <p>{{$food->excerpt}}</p>
        <span>{{$food->price}}$</span>
    </div>
    
    
</div>

</div> 
@endforeach


</div>


</div>





        </div>
    
      </div>
</div>
</section>


<section>


<h1 class="text-center">
    Gallery
</h1>


<div class="row">

<div class="col-lg-6 col-lg-4" style="margin-bottom:10px">

<img src="{{asset('template/images/job.jpeg')}}" alt class="img-responsive">

</div>

<div class="col-lg-6 col-lg-4" style="margin-bottom:10px">

<img src="{{asset('template/images/job.jpeg')}}" alt class="img-responsive">

</div>

<div class="col-lg-6 col-lg-4" style="margin-bottom:10px">

<img src="{{asset('template/images/job.jpeg')}}" alt class="img-responsive">

</div>

<div class="col-lg-6 col-lg-4" style="margin-bottom:10px">

<img src="{{asset('template/images/job.jpeg')}}" alt class="img-responsive">

</div>


</div>


</section>



<section style="margin-bottom:20px;">

<h1 class="text-center">
    Contact
</h1>


<div class="container" >

<div class="row" style="background-image:linear-gradient(to right,#3c65c4 , #1c2a33)">

<div class="col-lg-6">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5132298.749652971!2d5.9680312728407365!3d51.089910643069715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x68bee977894c93f9!2sGerman%20House%20Restaurant!5e0!3m2!1sen!2ssc!4v1619444238255!5m2!1sen!2ssc" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<div class="col-lg-6" >

<h1 style="padding-left:60px;color:#fdfdfd;">
    Contact us
</h1>

<ul style="padding-left:60px;list-style:none;color:#fdfdfd;">

<li style="color:#fdfdfd;">{{$item->address}}</li>

<li style="color:#fdfdfd">{{$item->email}}</li>


<li style="color:#fdfdfd">{{$item->phone}}</li>

</ul>
</div>

</div>



</div>

</section>





@endsection

@section('heads')

<style>

.bottom-image{
    display:none;
}

</style>

@endsection




